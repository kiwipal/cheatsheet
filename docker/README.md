
# Docker Cheat Sheet

**A collection of useful docker syntax.**  

Please contact me if you have questions or if you noticed a bug or a better way to improve.

Another great (better) cheat sheet is available on [dockerlabs](https://dockerlabs.collabnix.com/docker/cheatsheet/).

## Tips and tricks

### Docker on MacOS, file sharing

On MacOS, the folders used for the volumes must be granted an access in the file sharing preferences of the Docker Desktop, otherwise the OS will forbid the access.

### How to install a missing vi tool

On some distros, vi is not installed, one must log to the container to install it

    $ docker exec -it kiwipal-dev /bin/bash
    $ apt-get update
    $ apt-get install vim

## Lexical

### Docker hub

The official docker repo for images.

[https://hub.docker.com](https://hub.docker.com)

Free for public container.
5 $ / month for unlimited private repos
7 $ / month for teams

### Dockerfile
A Dockerfile is a text document that contains all the commands a user could call on the command line to assemble an image.

### Image
An image can be runned inside a container.

### Container
A container is a running instance of an image.

### Tags
Version of an image.

### Repository, Registery, image
Similar to Github but for images. For example, `kiwipal/demo` is a `demo` image from the repository `kiwipal` related to a registry (docker hub for example).

### Dockerfile
A file to store a list of command to run an image.

### Docker compose
A docker compose allows to run multiple containers and connect them.

## How to create an image release

All the examples below reference a `kiwipal/demo` image with a container named `kiwipal-dev`.

> The setup has been done, and the examples can be performed.

Dockerhub and Github must be connected.
Autobuilds must be activated in the Docker Hub with a build rule

    Source type : Tag 
    Source : /^([0-9.]+)$/ 
    Docker Tag : {sourceref}
    Dockerfile location : Dockerfile
    Build Context : /

After pushing code on github (origin/master), log on Github to create a new release named x.x.x (for example 1.0.3). As soon the release is created, Github automatically connect to Docker Hub to build an image with the specified tag.

> A slack notification can be delivered on Slack when the image is ready.

The image can be pulled and run with these commands:

$ docker pull kiwipal/demo:1.0.3
$ demo % docker run -p 80:80 kiwipal/demo:1.0.3

## Commands

### Docker hub

#### Infos about docker

    $ docker info

#### Login on docker hub

    $ docker login

#### Pull an image from docker hub

    $ docker pull kiwipal/demo

#### Push an image with a tag to the dockerhub

    $ docker push kiwipal/demo:tagname

### Image

#### Images list

    $ docker images

#### History of an image

    $ docker history kiwipal/demo

#### Building the latest image from a Dockerfile

> Command to be runned inside the folder where the `Dockerfile` is located, without forgeting the `.` at the end, which means that we are using current folder as a file source.

    $ docker build -t kiwipal/demo:latest .

#### Build a tagged image with a Dockerfile
    
    $ docker build -t kiwipal/demo:1.0.0 .

#### Update an image tag

> Will create a new image with the new tag.
    
    $ docker tag kiwipal/demo kiwipal/demo:tagname

#### Update an image from a container.

> A new tage must be specified

    $ docker commit kiwipal-dev kiwipal/demo:1.0.x

#### Export an image in a tar archive

    $ docker save -o archive.tar kiwipal/demo

### Containers

#### Show containers list

    $ docker ps -a

#### Build and start a container from an image

> This example run an image on port 80 and access to the server bash in a terminal window (`it` for interactive and `d` for detach).
    
    $ docker run --name=kiwipal-dev -itd -p 80:80 kiwipal/demo

#### Build and start a container from a Dockerfile

    $ docker run --name=kiwipal-dev -itd 

#### Start a container

    $ docker start kiwipal-dev

#### Stop a container

> This example use the name but the id of the container could be used as well

    $ docker stop kiwipal-dev

#### Build and start a container with a volume

> The `-d` option detach the process to avoid freezing the terminal.
    
    $ docker run --name=kiwipal-dev -d -v /Users/guillaumelenistour/Documents/www/demo:/var/www/html -p 80:80 kiwipal/demo

#### Delete a container

    $ docker rm [container name or container id]

#### Access the bash of a running container

> Container name is `kiwipal-dev` in this example, not to confuse with the image name.

    $ docker exec -it kiwipal-dev /bin/bash

### Docker compose

#### Build images without cache

    $ docker-compose build --no-cache

#### Run a docker compose file

> Command to be runned in the folder where the `docker-compose.yml` file is located.

    $ docker-compose up -d

#### Stop a docker compose file and remove containers

    $ docker-compose down

#### Check if containers are running

    $ docker-compose ps

#### Check the syntax of the docker-compose.yml

    $ docker-compose config

### Delete

#### Delete an image

    $ docker rmi kiwipal/demo

#### Remove all images without a container (caution)

    $ docker system prune -a

#### Remove all images (caution)

    $ docker rmi $(docker images -q)

#### Remove everything (image, containers and volumes)

    $ docker system prune -a

### Logs

#### Dynamic tail log of a container

    $ docker logs -f kiwipal-dev

#### Dynamic tail log of a container, restricted to last lines
    $ docker-compose logs -f --tail 5
