
# Command lines & Bash Scripting

**A collection of useful examples of command lines and bash scripts.**  
 
Please contact me if you have questions or if you noticed a bug or a better way to improve.

[[_TOC_]]

## Command lines

A nice website about command lines is [flavioscope.](https://flaviocopes.com/linux-command-ln/)

> Bash means "Bourne-Again Shell".

### Basic command and tools

Bash commands (like `ls` or `cat`) are located in the `/bin` folder of the OS.

#### Which shell?

	$ echo $SHELL

#### Who am I?
	$ whoami

#### Show datetime
	$ date

#### Sudo interactive
	$ sudo -i

#### Chain commands using semicolon.
	$ pwd;ls

#### Change password of any user using sudo (user `kiwipal` and pass `dhsbqs3` in example below).
	$ sudo passwd kiwipal 3QSD?dhsbqs3

#### Ping a server once
	$ ping -c 1 fr.kiwipal.com

#### Traceroute
	$ traceroute fr.kiwipal.com

### Important on MacOS

On MacOS, edit the `~/.zprofile` and be sure to add this command : source ~/.bash_profile 
It will automatically load the bash profile in the terminal session, otherwise you would need to type `source ~/.bash_profile` each time a new terminal is started.

#### Edit bash profile (bash config)
	$ vi ~/.bash_profile

#### Reload bash profile (first line is classic, second one use an alias)
	$ source ~/.bash_profile
	$ . ~/.bash_profile

### Help

Show help about a specific command (example below show the manual for the `ls` command)

	$ man ls

> Quit help using the `q` key

### Environment

#### Show all environment variables
	$ env

#### Get a variable (`HOME` in the example below) from the env list
	$ env | grep HOME

### Alias

Remember that alias are kept only during active session. In a sense, aliases are great in bash script, when they are not meant to persist.

> To persist aliases, you must edit the `~/.bash_profile` and specify the alias command inside the file.

#### Create a temporary alias (disappear when terminal is closed)
	$ alias curDir="pwd"

### Export variables

Just like aliases, exported variables won't persist unless being written directly in the `~/.bash_profile`.

#### Create a temporary variable
	$ export KP="Kiwipal"
	$ source ~/.bash_profile

#### Use a variable
	$ echo $KP

#### Show the home directory
	$ echo $HOME

### Paths

Paths are special variables because multiple path can be added one after another or separated with the `:` symbol. Bash will search for executable commands in the paths list.

**In a `~/.bash_profile`, this syntax**

    export PATH=$PATH:/usr/local/bin
    export PATH=$PATH:/usr/local/sbin

**is similar to ...**

    export PATH=$PATH:/usr/local/bin:PATH=$PATH:/usr/local/sbin

**But the first one is obviously easier to read.**

### Change the prompt command text

Just like aliases, changes on the prompt won't persist unless being written directly in the `~/.bash_profile`. 
	$ export PS1="Kiwipal $"

### Symbolic links

With hard link, the target will be removed if the orginal is deleted, but link can be done only on files. With soft link, the target won't be removed if the orginal is deleted, but link can be done on folders as well.

#### Create a hard link (on the desktop in the example below)
	$ ln cities.txt ~/Desktop/cities.txt

#### Create a soft link (on the desktop in the example below)
	$ ln -s cities.txt ~/Desktop/cities.txt

### Edit a file

#### Using vi
	$ vi cities.txt

#### Using nano
	$ nano cities.txt

#### Using text editor
	$ open cities.txt

### Navigate

#### Go to home (option + n)
	$ cd ~

#### Go to previous directory
	$ cd ..

#### Go to root directory
	$ cd /

### History

#### Show history
	$ history

#### Erase history
	$ history -c

### Print and echo

#### Print current folder path.
	$ pwd

#### Echo a simple text.
	$ echo "Auckland is nice!"

#### Print absolute urls of files in current folder.
	$ printf '%s\n' "$PWD"/*

#### Print content of a file.
	$ cat newzealand.txt

#### Print the numer of lines, words and chars of a file
	$ cat cities.txt | wc

### List

#### List all contents, including hidden files and directories, in long format, ordered by the date and time they were last modified.
	$ ls -alt

#### List all files in a new file.
	$ ls -al > list.txt

### Add content in files

#### Add a line of text in a new file (create if missin or overwrite otherwise).
	$ echo "Kia Ora" > hello.txt

#### Add a line of text in an existing file.
	$ echo "Kia Ora" >> hello.txt
	
#### Pass result to another command

> List files and use the pipe to chain a commande with the list, finally saved in a file.

	$ ls -f | cat > cities.txt

### Copy files

#### Copy multiple files.
	$ cp auckland.txt wellington.txt cities/ 

#### Copy multiple files starting with `auckland` and of type `.txt`.
	$ cp auckland*.txt cities/

### Move or rename files

#### Move multiple files.
	$ mv auckland.txt wellington.txt cities/ 

#### Move multiple files starting with `a` and of type `.md`.
	$ cp a*.md cities/

#### Rename a file.
	$ mv nz.txt newzealand.txt

### Delete files

#### Delete an empty folder.
	$ rm cities

#### Delete a folder and its files (and folders) recursively.
	$ rm -r cities

#### Delete a folder and all its files recursively (including . files) without confirmation.
	$ rm -rf cities

#### Erase the content of an existing file, and allows to enter new content (use `crtl + c` to quit).
	$ > hello.txt

### Sort and filter

#### Sort the content of a file.
	$ sort cities.txt

#### Reverse sort the content of a file.
	$ sort -r cities.txt

#### Keep only unique values from a list and save the sorted list in a file

> Uniq processes adjacent values only, hence the initial sort.

	$ sort emails.txt | uniq > filteredEmails.txt

### File owner

> Use the `sudo` command to get root authority if needed.

#### Change file owner to `guillaume`.
	$ chown guillaume cities.txt

#### Change file owner to `guillaume` in the group `kiwipal`.
	$ chown guillaume:kiwipal test.txt

#### Change file owner to all files from current folder recursively.
	$ chown -R guillaume .

### File permissions

**In the example `drwxr-xr-x`, the first char can be:**

    d : a directory
    f : a file
    l : a link

**The codes are the follwing:**

    0 no permissions
    1 can execute
    2 can write
    3 can write, execute
    4 can read
    5 can read, execute
    6 can read, write
    7 can read, write and execute

#### Everyone can read, write and execute
	$ chmod 777 filename

#### Owner can read, other can write and execute
	$ chmod 755 filename

#### Owner can read and write, other can read
	$ chmod 644 filename

> Use the `sudo` command to get root authority if needed.

### Search in file

#### Find all lines equals to `auckland` in a file.
$ grep 'auckland' cities.txt

#### Find all files containing the word `Auckland` in current folder
	$ grep -Rl 'Auckland' .

#### Find all files containing the word `Auckland` and list the occurences.
	$ grep -R 'Auckland' /cities

#### Find all lines equals to `auckland` in a file, but case insensitive.
	$ grep -i 'auckland' cities.txt

#### Use regex to find the lines starting with `guillaume@` or `glenistour@`.
	$ egrep -i '^(guillaume@|glenistour@)' emails.txt

#### Use regex to find the lines finishing with `@gmail.com` or `@gmail.fr`.
	$ egrep -i '(@gmail.com$)' emails.txt

#### Use regex to find the word `new zealand` or `new zealand` in every lines.
	$ egrep -i 'new-?zealand' countries.txt

### Find

#### Find all `.js` files from current folder (recursive)
	$ find . -name '*.js'

#### Find `cities.js` file from document folder (recursive)
	$ find ~/Documents -name 'cities.js'

#### Find `cities.js` file from document folder (recursive) and delete it
	$ find ~/Documents -name 'cities.js' -delete

#### Find the directory `cities` from current folder (recursive)
	$ find . -type d -name cities

#### Find markdown files excpeted in the `node_modules` files
	$ find . -type d -name '*.md' -not -path 'node_modules/*'

#### Find files edited in the last 24 hours
	$ find . -type f -mtime -1

### Find and replace

#### Find and replace all occurences

> Using substitute `s/` and globally `/g`) `Auckland` by `Wellington` in a file

	$ sed 's/Auckland/Wellington/g' cities.txt 

### Tail

#### Show changes
	$ tail -f /var/log/system.log

#### Show last ten lines of a file
	$ tail -n 10 cities.txt

### Process

#### List all process (not only terminal ones)

> The `ww` option allows to use word wrap 

	$ ps axww

#### Find a specific process
$ ps axww | grep "Visual Studio Code"

#### Kill the specific process `29540`
	$ kill -9 29540

### Crontabs

Use the [cron tab generator](https://crontab-generator.org/) if needed

#### List current crontabs
	$ crontab -l

#### Edit crontabs
	$ crontab -e

## Bash Scripting

**Script files have a `.sh` extension.**

> When saving the script file, it is good practice to place commonly used scripts in the `~/bin/` directory. 

**Every bash script file must start with this line:**

    #!/bin/bash

> A script must have the execute privilege to be runned, so use the `chmod` command if necessary.

#### Run a script
	$ ./getCities.sh

#### To avoid using the `.sh` suffix, create an alias in `~/.bash_profile`.
	$ alias getCities='./getCities.sh'

#### It is also possible to define an alias with default parameters
	$ alias getCities='./getCities.sh "Auckland"'

## Comments

#### Single line comment
    REM My comment

#### Multiple lines comment
    : '
    This is a
    very long comment
    in bash
    '

## Variables

#### Create a variable
    CITY="Auckland"

#### Get a list of files in a variable
    files=/cities/auckland/*.txt

#### Echo a variable
    echo $CITY

#### Echo a number in a string
    count=5
    echo "The number is $count"

#### Echo a variable in a string
    country="New Zealand"
    echo "I live in ${country}"

#### Create and print array
    cities=('Auckland' 'Wellington' 'Tauranga')
    printf "%s\n" "${cities[@]}"

### Comparaison operators

#### List of numeral comparaison operators

| Description           | Operator |
|:----------------------|:---------|
| Equal                 | -eq      |
| Not equal             | -ne      |
| Less than or equal.   | -le      |
| Less than             | -lt      |
| Greater than or equal | -ge      |
| Greater than          | -gt      |
| Is null               | -z       |

#### List of string comparaison operators

| Description           | Operator |
|:----------------------|:---------|
| Equal                 | =        |
| Not equal             | !=       |

#### If/else condition
    nbLocations="8"
    if [ $nbLocations -le 5 ]
    then
        echo "Please add more locations"
    else
        echo "You have ${nbLocations} locations"
    fi

#### Comparaison of strings (use quote to prevent empty values)
    city1="Auckland"
    city2="Wellington"
    if ["$city1" == "$city2"]
    then
        echo "Different cities"
    fi

### Loops

> Note the missing `$` in the for `city` as it is a var declaration

    cities=('Auckland' 'Wellington' 'Tauranga')
    for city in "${cities[@]}"
    do
        echo "I love ${city}"
    done

#### For loop on files
    for file in /cities/*
    do
        if [ "${file}" == "/cities/auckland.txt" ]
        then
            countNameservers=$(grep -c nameserver /etc/resolv.conf)
            cat file
            break
        fi
    done

#### For loop incremental
    for (( i=1; i<=5; i++ ))
    do  
        echo "Welcome $i in NZ"
    done

#### While loop
    index=1
    while [ $index -lt 5 ]
    do
        echo $index
        index=$((index + 1))
    done

### Get datas from user

#### Prompt
    echo "Enter your name:"
    read name
    echo "Your name is $name"

#### Access command line options
    REM Call script this way : `./cities.sh auckland wellington`
    echo $1 $2

#### Access command line options with a loop
    REM Call script this way : `./cities.sh auckland wellington tauranga`
    for color in "$@"
    do
    echo $color
    done