# Programming languages cheat sheets

Cheat sheets I have written as memos for differents programing languages, frameworks or tools I use.  
Useful as well to teach some tips and tricks to other team members.

Feel free to use and contact me if you spot any mistake or improvement.

@kiwipal