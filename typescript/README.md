# Typescript Cheat Sheet

**A collection of useful examples of Typescript codes.**  

Snippets are available on `typescriptlang.org` to get compilation errors displayed while a classic JSfiddle would directlty output builded result even on a typescript mode.

> All results are printed to the console and not displayed directly on the page. 
  
Please contact me if you have questions or if you noticed a bug to fix.

[[_TOC_]]

## Annotations

### Primitives annotations

Basic demonstration of primitives annotations (`number`, `string`, `boolean`) for variables. Declaring a variable without value and annotation means that the variable is of type `any` and can be later assigned any kind of value. 

> A value can be assigned imediately or later.

[Try this code on typescriptlang](https://www.typescriptlang.org/play?#code/DYUwLgBAngXAdgVwLYCMQCcDcBYAUFCAXggFYdc9RIAPeZNdIiAZnKogDMBLdAZzAByAQyQgY-dFzgBzJgHIA6l2DAuIuW3AQuvAIIBjMFwBuQsCAAmMFAHsboIXCZh0CEOUpbHUANJSL5N5+cBbyuhBwXPogEBZCUBp4Qf5MAIweuPo2cLz2IAB0wDbSABRQADQQ1JXcfIIiIJU6Bkam5haVySEAlJhAA)

```javascript
let y:number;
y = 5;

let x:number = 3;
let firstName:string = 'William';
let isActivated:boolean = true;

let anyKind;
anyKind = 'A nice day';
anyKind = 1;

console.log(y, x, firstName, isActivated, anyKind);
```

### Array annotations

An array with all elements of the same type is annotaded with a `[]` after the type declaration.

> An alternative syntax `:Array<type>` exists.

[Try this code on typescriptlang](https://www.typescriptlang.org/play?#code/DYUwLgBAxglmMgM4C5FgE4wHYHMDaAuhALwR4DkA6iMMNjmAPZbkA0E5AggK5QDWwAIZYAJmw4AVQd3TCcg8gQDcAWABQoSLPi5kWbgFsARiHSESZAIzsATOwDM7ACzsArMvWaIALxgAHZAhOdFkATwAefWNTAD4LPAB2VwAGZNd2JNSANg8NcAgYRE4AN0EYISNQZCNGRlBhc1I8DG4QdgAzQWBENogWkFz1KGZEOpAAOmBGHAAKWHgkdm16dl8-dkKSsorQAEolIA)

```javascript
let cities:string[] = ['Wellington', 'Auckland', 'Tauranga'];
let rating:number[] = [1, 2, 3, 4, 5];
let zip: Array<number> = [75005, 75006];
let isAvailable:boolean[] = [true, false, true];

console.log(cities, rating, zip, isAvailable);
```

### Multi-dimensional Array

Multi-dimensional arrays with elements of the same type can be annotated with multiples `[]` according to the array's depth.

> Example below illustrates two and three dimensional arrays. 

[Try this code on typescriptlang](https://www.typescriptlang.org/play?ssl=19&ssc=26&pln=1&pc=1#code/DYUwLgBAxglmMgM4C5FgE4wHYHMDaAuoRALwR4CwAUBLeQOQCCArlANbACGWAJvQDQR6AFU7N03HJ3oF+1OgwCKzECCxoA9gHcsAoQHVunNtILUCAbmrUoG9RtAA6YBpwAKWPCQBKK1WqgkBpgABYg6ADCcAgoaJi4hIkEpOTydJQ0Cun0ACLMWCA82Hr0ESGYaFAh4lUycplZSipqmjolhljGpmm0sj2pDVl4TKwc3HyC9PogwMDYOGB2df0Kw6LiktKTABKcALYwwIu6ZoN9VJbWVLb2Ti7uwWGR0T4WQA)

```javascript
let cities:string[][] = [
    ['Auckland', 'Tauranga'],
    ['Queenstown', 'Wanaka']
];

console.log(cities);

let otherCities:string[][][] = [
    [
        ['Dunedin', 'Christchurch'],
        ['Queenstown', 'Wanaka']
    ],
    [
        ['Auckland', 'Wellington'],
        ['Tauranga', 'Hamilton']
    ],
];

console.log(otherCities);
```

### Tuple array

In Typescript, a tuple is an array where each element has a defined type. 

A Tuple inherit the array's available methods and can be updated (with values replacements of same element type). However, keep in mind that an extended tuple will become an array and will no longer be a tuple.

> The last line of the example below will throw an error at build because elements can't be added without a `push` method to protect the tuple integrity.

[Try this code on typescriptlang](https://www.typescriptlang.org/play?ts=4.1.3#code/DYUwLgBAxglmCeAuA2gZzAJxgOwOYBoJsBXAWwCMQNDyB7W0AQ2wF0IBeCZAcgEFioAa2DMAJt0IBWQpmIgWAbgCwAKCi1sqBiAB0wWrgAUsBAEpVqk-GQAGNp24B1EMGA5cYDd2VqNW0HoGxnDw5iqWIToADsSoABaG3AASjKQwwJ7Y3KY+6pragUZWYREIyAAs9hDcACqMxBjMuIzcQA)

```javascript
let city:[string, number, boolean] = ['Auckland', 5, true];
console.log(city)

city[0] = 'Wellington';
console.log(city)

city.push('Hamilton');
console.log(city)

city[4] = 'Tauranga'
```

### Object annotations

Object annotation belongs to the most useful concepts in Typescript, allowing any kind of anotations for every key (even enums are available).

The second example below with the `getCountry` function illustrates that object annotations can also be used as function argument.

The last example below with the `mustDo` array demonstrate how it is possible to both declare and assign at the same time, even for an array of objects.

[Try this code on typescriptlang](https://www.typescriptlang.org/play?ssl=1&ssc=1&pln=30&pc=21#code/DYUwLgBAxglmCeAuCBvAUBTEB2BDAtiMgM5gBOM2A5gDQZYAOA9gwK7C5gxPbLav4ARiDJ0sEABa5iAQRhlmZMMkFMmoXNjFYqDYshQdl-ISJoRgPKohPCyAX22YAbjGJx9pCtQDaAXTR7AG40NFgECABeVHpMPEJkACIZVigAaw5sABNEpwhmNiNuXgBGADYABjKAVjKAFjypWXlFZXJWEDzdfVQjRABaAGYygDoADjqJspLzS2pEEoB2OpHFssGNssdYiFd3MB6fAHIAZTT4CAAVJgB3ESPzI4BZXG9cB4gjgHFXrJBsYhHAL2UJQHjEdQgEaWKgACnC8AAlCE0KBIFRwABhJisbDkC7ReE4vFkJCoeJECBeSi0fIsdicYp8AR2eyIyIAPggAAMACQoMG4-EjCn2STSCD8wUk+AjAoMrg8MUMEAsUAjbkosEAyHQphwiAYsDYoWk2EoCmII4pdKZLIfeVFHiISZjSoVCpsiDI0JoiD4VikAAiTEQFoIRGp1BoZEZ81sIns-iiEB8O3DCU+Noymnt5ljXHmEGq23EGcpRwA6iBgMAaWAeB8CzTkHUQX4UQHg0w5YGJLDyRHkEdLrhWLHqO983HrBBBmKfWFwbqYbCu2AQ8igA)

```javascript
let city: {
    name: string,
    population: number,
    hasAirport: boolean,
    gps: {lat:number, long:number},
    visits:string[]
};

city = {
    name: "Auckland",
    population:1606564,
    hasAirport:true,
    gps: {lat:-36.848461, long:174.763336},
    visits: ['Sky Tower', 'Marina', 'Gardens']
}

console.log(city);

let getCountry = (country: {name: string, population: number}) => `${country.name} has ${country.population} people.`;

console.log( getCountry({name:'Auckland', population:4886000}) );

let mustDo:{name:string,rating:number}[] = [
    {name: 'Auckland', rating: 5},
    {name: 'Wellington', rating: 4}
];

mustDo.push( {name: 'Tauranga', rating: 3} );

console.log(mustDo);
```

### Object alias

The example below builds a list of friends using aliases of object annotations. Notice than after the list of friends, a city object is built using the `Gps` object annotation also used for the friends (same object can be used for different purposes)

> Aliases are very useful to avoid repeating common annotations.

[Try this code on typescriptlang](https://www.typescriptlang.org/play?ssl=1&ssc=1&pln=42&pc=19#code/C4TwDgpgBA4mDOUC8UDeAbAhsAXAOwFcBbAIwgCcAaKdAezwHMcpDSKBfAbgChRIoAwvWCYAxsGRowAC3oQc8YOQCWjahCKZl6BUtUN23XuGgAFCvHqTU3KHZaYi8qIpVrb9hgmZx4lD3aiwmK4gsHi3Ibc6BASmDEAHph4ACYUOObklnjWAQ5OzADkAIKJyWnkhf72UF7wzBjYOAC0AMwAbAB0ABwALH3tAIzUdIw4gwDsvZ0T7a3z7ezV9kF4IuINeTUyckUADIO9c63dAKyDAExVW-YaWjpQhfEQSankEAACCd8JnUFEhTyhi4RhiEgA7tp0MpHBkLFYUDYanhHM5CgB1KEwgHLOx1BpYXBtLp9AbDGj0JiTaazeYdJZ5VbrUJImr2HZ4NF7dqnCYTVp8i6ta5s26abRFSHoaGOL4-P60AFAyI8aKxKAAMxUEFS9Uy2QA2gBdSTG1Va5Q6lLwTpgAjwaQACmerwq1ClMqIAEpVatLDFOnQGI6LVb4D7QerRMpQJtkajmK59Hl8bAEP52Ll4wUoAAiYoEUQAaywqVzuNq3jQhJaHR6-SO5NGVKmM2O9IzRj9tADQcd0dAPqAA)

```javascript
type Gps = {lat:number, long: number};
type Contact = {phone:string, email:string}

type Person = {
    name: string,
    gps: Gps,
    contact: Contact
}

let alexander:Person = {
    name: 'Alexander',
    gps: {lat:-36.848461, long:174.763336},
    contact: {
        phone: '0146338512',
        email: 'alexandre@xxxx.com'
    }
};

let william:Person = {
    name: 'William',
    gps: {lat:-36.848461, long:174.763336},
    contact: {
        phone: '0657737723',
        email: 'william@xxxx.com'
    }
};

let friends:Person[] = [];

friends.push(alexander, william);

console.log(friends);

let city: {
    name: string
    gps: Gps,
} = {
    name: "Auckland",
    gps: {lat:-36.848461, long:174.763336},
}

console.log(city);
```

### Object readonly

Objects (tuples) or arrays can use the `readonly` keyword to create real imutable constants.

In Javascript, using `const` does not prevent a value to be updated (with the `push` or `pop` methods for example), while the `readonly` keyword can prevent any kind of operations.

> In the example below, trying to assign a new tax value will throw an error.

[Try this code on typescriptlang](https://www.typescriptlang.org/play?#code/C4TwDgpgBACgTgSwMYQMIHsB2BnYBDTYbKAXigG8AoKGqOCPAEywBsQp8APALkwFcAtgCMIcSgF9KlJFlxQwiFN3jI0s-IWJlyXbgEYAnADoAbJJk50LCEZboA5gAoFqo1wCUAbikuUbvJykUABMAAyeQA)

```javascript
type PriceConstants = {
    readonly tax:number
}

const price:PriceConstants = {tax:19.6}
console.log(price.tax);

price.tax = 20;
```

### Object optional parameter

Missing parameters won't throw an error if a `?` symbol was defined right after a parameter name in the type definition of an object.

> The example below would throw an error if the `?` symbol was missing for the `population` parameter.

[Try this code on typescriptlang](https://www.typescriptlang.org/play?ssl=8&ssc=21&pln=1&pc=1#code/C4TwDgpgBAwglqKBeKBvAUFKA7AhgWwgC4oBnYAJzmwHMAaTKMAezAFcAbXYOZ7AfhLY2+AEYQK6AL7p0AYz7ko+EPFBE1IZGjyEiAcgCCbOQGsu2ACb6Z8xcw4QAdB2Y0AFCs0BKANxA)

```javascript
type City = {
  name: string,
  population?: number
}

const myCity:City = {name:'Auckland'}

console.log(myCity);
```

## Custom types

### Custom Generic Types

A solution to create collections of types sharing annotations.

[Try this code on typescriptlang](https://www.typescriptlang.org/play?ssl=19&ssc=25&pln=1&pc=1#code/C4TwDgpgBAwghgJygXigbwMYEtQGViLABcUAzsAlgHYDmANFNqAKJUAmJ5ltDAthAVJEu1GgG0AugF8A3AChQkWHF6QkqTCrCjOFUQypYaAC2BCoVAK68ARhASy5C8NAAqlMAB5XAPhRQxVwYgqFcJeTkAGwEoADMsBHJ3LDASZK94BD9UMTkodCYQfEISAHIAQUsMAGtIuHZShkLWDihSgAUsDGAAeypGqH5BIjFSmjBSUuk6PIKcIoIEYjbO7r6B5vYygEVLCAgqch6Ad36+ATghUfHJhlLSKhPGYzhqSenZzHnipZ29g6Opw28xaZRgxko5AwxksCGhAyGlxGYwmUykcnCTmiwDIEAwfTY6TSHk88FU9myAU+GC0OjaACUer0EJY4ANDCYzEQAKxSGb5TSqOmleDkaJgHrUYDsoymIQAZj51NptDKAHUIJFIqJemcLLKuQAmdGYuT4w49aIAOkiPRoAAp4olgOkAJTyc2kS0QG12+2kPEEt0yIA)

```javascript
type Car = {cityStart: string, cityEnd: string, metas:string[]};
type Camper = {camping: string, nights: number};

type Trip<T> = [T, T, T];

let firstTrip: Trip<Car> = [
  {cityStart: 'Auckland', cityEnd: 'Picton', metas:['gps']},
  {cityStart: 'Picton', cityEnd: 'Queenstown', metas:['gps', 'snow chains']},
  {cityStart: 'Queenstown', cityEnd: 'Christchurch', metas:['gps']}
];

let secondTrip: Trip<Camper> = [
  {camping: 'Rotorua', nights:5},
  {camping: 'Castlepoint', nights:3},
  {camping: 'Wellington', nights:2}
];

console.log(firstTrip);
console.log(secondTrip);
```

### Custom types from enums

Custom types can be used with function arguments if a new type is created based on an enum list.
This is not very convenient however, and relaying on `union types` would be easier.

> In the example below, the city `paris` being not defined in the `enum` will throw an error at build time.

[Try this code on typescriptlang](https://www.typescriptlang.org/play?#code/KYOwrgtgBAxglgFzsAzlA3gQTDA1gGwEMQATAGgHVh984QBzBAexDIGEALAJzhQRg5guAgL4AoMQgCeAB2CxEyFABleCKAF4ouYFKYAzKNLkGFSVAG4JMFnygRUKQvXlaAFPGkAuT0tV8ASk0APigAAwASdE8pESheKHouYEIEAEIwqzEbEDsIKTZFVB8ilTUAbQBdTShygHJsPCJSOrIoOqoaOkYWVvaABUIeFDrKiXzC8xQAOn0mLgBRQgEPRCkQ2FsmfGBp-CZ6NwcUJxdV6QDLiyA)

```javascript
enum cities {Auckland, Wellington, Christchurch}

type citiesList = keyof typeof cities;

const message = (city:citiesList) => `${city} is great!`;

const myCities:citiesList[] = ['Auckland', 'Wellington', 'Paris']

myCities.forEach(city => console.log(message(city)));
```

### Custom types using typeof

A new type can be declared using the `typeof` of an element.

> In the example below, the new type is the first key of the `cities` object, matching `{name:...}`. 

[Try this code on typescriptlang](https://www.typescriptlang.org/play?ssl=11&ssc=57&pln=1&pc=1#code/DYUwLgBAxglmMgM4QLwQNoCgIQN4DsBDAWxAC4ByAQQFcoBrYQ-AEwoF8AabPI0ygOohgwGPgDmYAPb4O3HARLkKAFUI0ATs3GEOmALoBuTJjABPAA4gIAYThnUEc1akAzaHASJ0ABiMnYeCQAOgsaRAALAApFfggKAEUaEBB8RGkAd1l2AEoAzxDXKQ0AUUIoaKjAszI7cxzUAD48KBlEKVBg4ClxKvtgvhAc3ONMIA)

```javascript
let cities = [
  {name:'Auckland'},
  {name:'Wellington'},
  {name:'Tauranga'}
];

type City = typeof cities[0];

cities.push({name: 'Queenstown'})

cities.forEach((city:City) => {console.log(city.name)});
```

## Union types

### Basic union type

Union types are great to replace enums for simple list of values.

> In the example below, calling the `getAnimal` function with a value not listed in the type `Animals` would throw an error.

[Try this code on typescriptlang](https://www.typescriptlang.org/play?#code/C4TwDgpgBAggdgSwLYEMA2BnKBeKByAawhTygB98CEB3BUiwlAlMAezwG4AoLgY1bgZgUAOYRg8ZOhxQAFCkSo0ALklKMAShwA+KAAMAklDSsAbtAAkAbwVS0AXwCEe7nwEZWaCADoTI2aLiauiyhDR0WhocQA)

```javascript
type Animals = 'kea' | 'kiwi' | 'kakapo';

const getAnimal = (animal:Animals) => `I love ${animal}!`;

console.log( getAnimal('kiwi') );
```

### Union type narrowing using typeof

Type narrowing using `typeof` (known as the `type guard` technique) is useful to perform different actions based on a specific type.

> The basic example below permute time duration between string format in hours and numeric format in minutes.

[Try this code on typescriptlang](https://www.typescriptlang.org/play?ssl=25&ssc=48&pln=1&pc=1#code/MYewdgzgLgBKYDcCmAnKARArighlAluBDALwwAUMAJtnoZAFznQr5gDmAPmJgLYBGqAJQBtALowhTFmy48Bw8aQB8MAN4AoDTB0wANklgokETHqgRpUVh259BKURLLiA3Ft3VaBIgDoAZiAoAKI4wAAW5PhQSLwq6tqeuvj+FFAAngAOSCCp0bGkZADkMhxFQglJVTApFJk4KBBIAJJgUFExvEK+UCAAytay5BUA1DBF4UWFNZ0VmtULMMam5hC+mZgQkfWNLW0dsRUAVDAAbAAMQu6LugC+SHpN8zdJy2YW65uRAAYAJGr5Xi3GAMGCtBA4PT4KgzAqldgwQIoXh4AA0MF4m1gghgInkDjE4W+V0SN1upKS90eSGeNzeq0+W3IBziAHozpcev1BhxhjAxhNytdquTPLchBTjFBsGAliZ3hB3OSNPBoBi2JgYhAsLgfIx8aglC4LuiAIwAJnOZoAHOcxO5VbBwiBsNrvPRLPCjbiiqbJuiiub-eMAMyTe0qoiwAi8JA6uhEBhe5w+v1FANBpDYJDp0Ph9yRyAgAy+PQgdiUeDINDxvUQci8DVa2seiok1XFpCl8uV8DVjDuojkZ2ultENsOoid7sVuB91AD3Ue8gxuODyAToA)

```javascript
const convertDurations = ( durations:(string|number)[] ):(string|number)[] => {

    let results:(string|number)[] = [];

    durations.forEach(item => {
        if (typeof item == 'string') {
            if (parseInt(item).toString() + 'h' == item) {
                results.push(parseInt(item) * 60);
            }else{
                results.push(`${item} : Invalid item string format, must be [number]h`);
            }
        }else{
            results.push((item / 60).toString() + 'h');
        }
    })
    return results;
}

const minutesDurations:number[] = [60, 120, 180];
const hoursDurations:string[] = ['1h', '2h', '3h'];
const timeDurations:string[] = ['1h', '2heure', '3h'];

console.log( convertDurations(minutesDurations) );
console.log( convertDurations(hoursDurations) );
console.log( convertDurations(timeDurations) );
```

### Union type narrowing with the `in` operator

The `in` operator is useful to perform different actions based on the existence of specific functions or value.

> The example below will make a pet run or swim according of its kind.

[Try this code on typescriptlang](https://www.typescriptlang.org/play?ssl=34&ssc=32&pln=1&pc=1#code/C4TwDgpgBAwghsKBeKBvAUFKA7OBbCALigGdgAnAS2wHMBuTKcgV22IAoBKZAPlIur10AX3TpQkKADFKJABbI0jXAWJkqtBlhIB3Sng7ckfdYIaj0AYwD22MlEjkScNvEQpUUZfiJQA5AAq1swANsEkEH4ANF5YLGxQXLz+ULJMrNiCAIR+ImI2doiWYTrYMvKE5QoesTg+xH4AchB41tG1uvqGyX6pJKR6eHjZuRYF9q0AbtAo7JDAxG5QAD7SsnKchKa0yRiMlABmiX7xvdQOEMDcGFhxl8zk2BfAAHQq0ADUzy-xXFpQoiwh2OnTwZye82ujDuwAeEMubx8UC+8xeoL+jAsMLhUAARAAFS5QAAmlGJOGsiCmECyuPM+VsJGsIQgLzCNHYUGpcwgThc3E4DHGzNZ7M53OK1lKVQFdCAA)

```javascript
type Cat = {
  name: string;
  run: () => string;
}

type Fish = {
  name: string;
  swim: () => string;
}

const persan:Cat = { 
  name: 'Toulouse', 
  run: () => ' is running!'
}

const clownFish:Fish = { 
  name: 'Nemo', 
  swim: () => ' is swimming!'
}

const move = (pet: Cat | Fish):string => {

  if ('run' in pet) {
    return pet.name + pet.run();
  }
  if ('swim' in pet) {
    return pet.name + pet.swim();
  }

  return "Pet did not move!";
}

console.log( move(persan) );
console.log( move(clownFish) );
```

## Interfaces

### Basic interface on objects

Interfaces are similar to `type` declarations but without the `=` assignment and are restricted to objects.

> Interfaces are mostly used with classes but can be used for classic objects as well.

[Try this code on typescriptlang](https://www.typescriptlang.org/play?#code/JYOwLgpgTgZghgYwgAgMLDATwN4ChkHIhwC2EAXAM5hSgDmANPoQA4D2LArgDZxjBsQ5EJxIAjaLgC+uXNwhhkJTOizlVmZAF5k2YmXIByAIKcEAa14gAJoYbJ2XXv0HkAjADoAbFIDcshEFKNnkPbjY6AAplDQBKXyA)

```javascript
interface City {
    name: string,
    population: number
}

let myCity:City = {name:'Auckland', population:1.6};

console.log(myCity);
```

### Class interface

Interfaces are the perfect match for classes. It is also possible to split the interface in multiple interfaces using the `extends` keyword. It is even possible to use interface compositions (see how the `Country` interface is defined in the example below).

The `private` modifier is available, but private properties must not be defined in the interface.

> In the example below, the final `console.log` will leverage an error while compiling typescript because the method is trying to access a private var while a `getSecretLocation` method should be used instead.

[Try this code on typescriptlang](https://www.typescriptlang.org/play?#code/JYOwLgpgTgZghgYwgAgFIHsCuUQQJ7IQAekIAJgM7ICCwUADulGADTIDCwYwEVA3gChkw5AizgoeAFztxYSQG4hIgOYQwAFS4AbCFOQAKBFzwBlMHGZSK80CrbGweAKLlrtkCoCUyALwA+ZBsoOyUAXwEBUEhYRBRaBiYwZEERZDUwBMYrQxA4AFs9YLs2YAoASXBoPO50PO0pACN0dF04EB8AoI8VAQioqtikDi4efmVhDM4nfQM8wvcQzzZGekxtOFqQKRBMfMboTsDizz7I6Oh4YdlMCQJUkXminqU0svRF0LOBBA2KKg0UDgADcINpkMB8vRdIVwFQMNhcHhBBNRHJJFIHmlhE99Cd7Ki3hQPviWKiwkpUfQQsDNigAPoUCAIKDqAAy6AQm2AdU+nkpaTEIGCmAQYCYhjEt3k0hudzYTJZ7M53N5+J8WOxYAAFmUAHRSu5+NHSxSEkQ6-WM5mssAcrlbY2K2321UgV4ifppDJaMC6IwmcyWMB8+yiEyuMihjXm4S2xHIAAGABI+JaKAb0Xg9U8wsh5CCwch9KnHGYLMw8+LkKWI+QwomPcIvap1FkknMCs8lmGypUYjUefUmi02h0UrHkPGcEnU7nkHA6Nlkqm+4NB3U4ODfL581BMCgAPzIADkTKgoKoFxwbq3J+Lp-Pl+QZHQhRswAQJ7zMG0wBU2pgBmjbkpE3rqNMeCdgspLIKs6xujsewHFAMbYiI04gLOfDztqcBUHAcHoGsGyOugMA1nw8GkUOeb0NAxLCnqIFpC2kzqKYNrKg6Q4GGh6FTuoCbpnq1pKnaKpbE2yD9P0ujJPkeCAoW27ILgADuyDKaC2gGDhXZSCeAByECaQAWhAW7tGQJ6lMShlGWZ35sCeGgoAAEnAawnl4lJCsSuh6to6AqAYyCKdpYJ6j6OgQAYJ7UKKADWGzkLZp4AOpgn+njiiAPnIL5Px1AFEBBSFYURUCOnRW2S4dsZYKMel8DaEyPhFf5rRlcFoXhUp1VRWJLqSUOvlAA)

```javascript
interface Journey extends Airport, Cities {
    country:Country;
    getTitle: (cityStart:string, cityEnd:string) => string;
}

interface Airport {
    getAirport: (name:string, isInternational:boolean) => string
}

interface Cities {
    getCity: (name:string, population:number) => string
}

interface Country {
    name:string;
    iso:string;
}

class Travel implements Journey{

    country:{
        name: string,
        iso:string,
    };

    private _secretLocation:string;

    constructor (country:Country, secretLocation:string) {
        this.country = country;
        this._secretLocation = secretLocation;
    }

    getTitle(cityStart:string, cityEnd:string) {
        return `${this.country.name} travel : ${cityStart} to ${cityEnd}`;
    }

    getAirport(name:string, isInternational:boolean) {
        return `${name} airport ${isInternational == true ? 'serves international' : 'serves domestic'} flights.`;
    }

    getCity(name:string, population:number) {
        return `${name} has a population of ${population} persons.`;
    }

    getSecretLocation() {
        return this._secretLocation;
    }

}

let myTravel = new Travel({name:'New Zealand', iso:'NZ'}, 'Te Hapu');

console.log( myTravel.getTitle('Auckland', 'Wellington') );
console.log( myTravel.getAirport('Nelson', false) );
console.log( myTravel._secretLocation);
```

## Functions

### Function Types

Rather than defining types for similar function arguments, it is sometime better to define a function type and assign it while declaring each function.

In the example below, the generic `getHeight` function type is assigned to annotate two different methods using a string and a number as arguments and returning a string.

The last example is valid but shouldn't be used as it is a bit cryptic (a developer would have to search for the function's type to get the list of object keys). The point with a function type is not really to shorten the code, but to avoid to repeat annotation for similar functions.

> The name of each argument in the function type is not important (in the example `a`, `b`), any word could be used.  

[Try this code on typescriptlang](https://www.typescriptlang.org/play?ssl=9&ssc=45&pln=1&pc=1#code/C4TwDgpgBA5hwAkIEsYAthQLxQBQEMAuAZ2ACdkA7GAGigCNDKBXAW3ojIEpsA+KUhWoAoYQGMA9pVKx4AWXyUkqDITiIU6TDlytFdNJow8s-AAYASAN57KAXyjJiUa4ZWYA9FACMABl8OrPCczm5aAHRmANziUjLqchLMlMD4VMpaavAZGNh4rEkpaZQGRsAm5tYFyalUDk4uVmEYgcFkoWWRMZLSmOoAQszIADYAJlQwWRruebjh8-gVUJZW+ADavgC69c7W697bUEHAIVDNwF2iPcQSwxDhwxIwuAmKOcC4AOQAgncAHopRpxPnRvABOAAsPC43Tit3uj2eCUKtSUZS+ckwAGEJBIANYgqAAZgA7AAmKFQGGxaTwh5PF7wQYjcbUL4AZTxICgABUJAB3YF0IlkgAc0KiQA)

```javascript
type getHeight = (a:string, b:number) => string

const getManHeight:getHeight = (man, height) => `${man} is ${height / 100} meters height.`;
const getMountainHeight:getHeight = (mountain, height) => `${mountain} is ${height} meters height.`;
const getBuilding:getHeight = (...a) => `${a[0]} is ${a[1]} meters height.`;

console.log(getManHeight('Alexander', 194) );
console.log(getMountainHeight('Mt Cook', 3724) );
console.log(getBuilding('Sky Tower', 328) );
```

### Function optional parameter

Missing parameters won't throw an error if a `?` symbol is defined after a parameter name. It is also possible to define an optional parameter by assigning it default value (the second example below illustrates the concept).

> It is not possible to use both `?` and a default value at the same time.

[Try this code on typescriptlang](https://www.typescriptlang.org/play?#code/MYewdgzgLgBAFgUwDZJDAvDAFGAhgWwQH4AuGaAJwEswBzASgwD4YADACWVQBoYASAN55CMAD6iYAcnwBPGADNqCMABNJAX1YBuAFA7QkEEgQA6VLSzwuILJICCxgB65VCCpMb1dBiEdPnLRBQbT119cGgrYIAmDGxhBDJKGlo4gCI0xnQWDmteQQSxCWk5RSplNU0wnz8zEAso1GjbACkQODAPGC9ww2M6hqCmrFCgA)

```javascript
const hello = (name?: string) => `Hello, ${name || 'my friend'}`;

console.log( hello('Alexander') );
console.log( hello() );

const hello2 = (name: string = "") => `Hello, ${name || 'my friend'}`;

console.log( hello2('William') );
console.log( hello2() );
```

### Function argument annotation

A function can use annotations for its arguments just like variables declarations.

[Try this code on typescriptlang](https://www.typescriptlang.org/play?ssl=3&ssc=45&pln=1&pc=1#code/MYewdgzgLgBA5gUygYQJZQJ4wLwwBRgCGAtggFwzQBOqYcANDFYVLXBWAK7EBGCVjVBABKCUMVJgAJgikUeIEABsEhMAEocAPhgADACQBvIqQC+MIUxayYR5qzrm1UixFuGho8ZJkuA-DAA5FAgMHwwAG5C6LKBMBSBYCCwIWEIkdFQsaYAdLoA3ACwAFAloJDKCDlKIHB48EhomHiBAIKcwADWSs6BjACsjFBUnAia6vlAA)

```javascript
const getCity = (name: string, rating: number, isRecommended: boolean) => `${name} is rated ${rating} and is ${isRecommended ? 'to be visited' : 'not to be visited'}.`;

console.log( getCity('Auckland', 5, true) );
```

### Function annotation inferring

The type of a value returned by a function is checked in case of variable assignment. To double the security, it is also possible to define the type of the value returned by the function. 

The second example below will throw an error because the value returned by the `getPrice` function is of type `string` and not `number`.

> The third example illustrates the syntax for classic function declaration.

[Try this code on typescriptlang](https://www.typescriptlang.org/play?#code/MYewdgzgLgBA5gUygJQIZQJZjjAvDACgCd0s4AuGMAVwFsAjBIgSjwD4YADAWQE8YSmbDAwQYAEgDegsgF9OAbgBQAGyQDS2ctCJk88JGiFwCAZmbKloSLERQACruAJ9BAA5OElGgyattULrCuBycACoAFi4eGM4iYlIxzrISiqrqSV4+jET6dkZkZhZKSgBm1GDAmOAGUAAi1DLgBBAI1gAmEN50OcyUOnqSSjAjAkiNYFxSrR0QKTPgnWmyymqw7Y2k4AFBOPh2DU1gBACsFkA)

```javascript
const getRating = (rating: number) => `My rating is ${rating}`;
let rating:string = getRating(3);

const getPrice = (price: number) :string => `The price is ${price} $`;
let price:number = getRating(3);

function getDuration(seconds: number):string {
    return `${seconds} seconds`;
};
let duration:string = getDuration(5);
```

### Function without a returned value

A function not returning a value must specify a `void` return type or an error will be thrown.

> The second example illustrates classic function declaration.

[Try this code on typescriptlang](https://www.typescriptlang.org/play?#code/MYewdgzgLgBANiA5gYQJZQJ4wLwwBTDoYBc0ATqmIgJTEBuIqAJjgHwyiQhwCmAdAkQEi1ANwAoQWkx4A5AEEArsADWcAIZgmssePEAzRWGBRU4eEnkmzYPOuvhSUClVoNmMAN7iYvjuAhufkE7BzBdAF9JSzC5RQAHJnUoHm0xIA)

```javascript
const logCity = (city:string):void => console.log(city);
logCity('Auckland');

function logAction(action:string):void {
    console.log(action);
}
logAction('updated');
```

### Function using spread operator

The spread operator is useful to pass tuples as arguments to a function with many arguments.

> In the example below, the getItinerary method should be called using six parameters, but using a spread operator on the tuple gives a more elegant and readable code.

[Try this code on typescriptlang](https://www.typescriptlang.org/play?#code/C4TwDgpgBAwglqKBeKBtAzsATnAdgcwBpcBXAWwCMItjyqsBdAbgFgAodgYwHtdMpOCEAEYAXPEQpUAcgCCJTgGsANgENcAE2mEoAWgDMANgB0ADgAsFw8J3CA7OeN3D+14eZde-QaABM4oWQ0aQB1CGVlPHxgXm09fTtjQ1M7OwBOU1tnY2FDAFZhfWEPDjYAMxJcTmA4Xih8CGAASRrcalUsEAAKdigoTA7gCRBRTBwCQl7+4EGAGVVgUVJKakm2PoGsYFnefCW6VamITWHR7Ci1vuONecXl+kuoa52CfZWsdgBKUQxziah7qsAQdGKgGFAAN5TPpYRokLC4NC-QbDHSbbYLNEzLYvfAMHSoa6op6aW46Z67BglAC+7E8fGAAm43CwGjwCwg6CCDWarXanS6xiFPhEOiFxhFvk+rFKPD43GUEGMym4+C6PBZbNwHPQ0qAA)

```javascript
type City = [string,number,number];

const city1:City = ['Auckland', -36.848461, 174.763336];
const city2:City = ['Wellington', -37.687798, 176.165131];

function getItinerary(
  startCity:string,
  startLat:number,
  startLong:number,
  endCity:string,
  endLat:number,
  endLong:number
):[string, number, number][] {
    return [[startCity, startLat, startLong], [endCity, endLat, endLong]];
}

const coordinates = getItinerary(...city1, ...city2);

console.log(coordinates);
```

### Another function using spread operator

Another simple way to display a list thanks to a `forEach` and a spread operator.

[Try this code on typescriptlang](https://www.typescriptlang.org/play?#code/MYewdgzgLgBBAWIDuAlAhlAlmA5jAvDABTCZQCeAXNAE7Y4A0MNG9lYArgLYBGApjSZQQAExCUeIEABs+aMAEpKANxCYRBAHwwA3gFgAUDBihIMvgDppIHEQAGAEh2kKAXxiYIzDHw1OWWLju8hpcHNAwTsJiMAD8MADkCTCUiWAgUAnu-DDKnmS+MACEdgoA3IauhsaGsrABbDAA2rT0TJy8AkyS5vIAuk19BM3VzQkAghzAANbSIQlMACxCNBx8fQyjTQkA6nzS0vTCYAswAKwraxtbCQAqaBwsuGinAMyX65tGYwASaFyYaTHU4AJiYADM0NIIJ9DH0Kt9DA1cBZwSAaABRNDAeBEApcLRwRCoVi4IgWCn4hTlIA)

```javascript
const showRating = (city:string, rating:number, todo:boolean):void => {
  console.log(`${city} is rated ${rating} and must ${todo ? '' : 'not'} be visited !`);
}
  
let rating: [string, number, boolean][] = [
  ['Auckland', 4, true],
  ['Wellington', 5, true],
  ['Tauranga', 3, true],
  ['Hamilton', 2, false],
];
  
rating.forEach(item => showRating(...item));
```