# Javascript Cheat Sheet

**A collection of useful examples of Javascript codes in ES6.**  

Each sample has a link to a JSfiddle to be able to test the code online.  
Find all the [codes on my profile page](https://jsfiddle.net/user/kiwipal/fiddles/)
 
All results are printed to the console and not displayed directly on the page.
  
Please contact me if you have questions or if you noticed a bug or a better way to improve.

**Useful pages**

> https://getfrontend.tips/all.html
> https://github.com/ryanmcdermott/clean-code-javascript

[[_TOC_]]

## Promises

### Basic example

Basic demonstration of an `async` promise returning the state of a button, illustrating the `resolved` and `rejected` mechanismes.  

> This code is also functional without the async word.

[Try this code on jsfiddle](https://jsfiddle.net/kiwipal/Le9c8vzn/3/)

```javascript
const isButtonActivated = true;
	
async function getButtonState() { 
    return new Promise(
        (resolved, rejected) =>
            isButtonActivated
                ? resolved("On")
                : rejected("Off")
    )
}
	
getButtonState()
    .then(resolvedValue => {
        console.log("Button is",resolvedValue);
    })
    .catch(e => {
        console.log("Button is", e);
    })
```

### URL fetch requests, waiting for the result

Asynchronous fetch of an url, waiting for a JSON result to log a key/value pair.

> The `await` operator can only be used with an the `async` operator assigned on a function.

[Try this code on jsfiddle](https://jsfiddle.net/kiwipal/q9x3f874/5/)

```javascript
const getCurrencyRate = async (url) => {
    
    const result = await fetch(url);
    
    if (result.ok) {
	
        const json = await result.json();
        console.log(json.response.nzd)
	
    } else {
        throw new Error("Invalid URL!");
    }
    
}
	
getCurrencyRate( 'https://api.kiwipal.com/public/getCurrencyRate' );
```
	
### Chain URL fetch requests (async and await)

This example build an object using the `async` and `await` operators to chain a list of fetch requests. Each request will wait for the previous one to be finished.

A call to the `waiter` function in the `getRental` method demonstrate how it is possible to wait for a specific amount of time (using a method returning a promise with a setTimeout function call).

Check the console and you'll notice that the method `getJourney` run about five seconds later, demonstrating that the queries are chained.

> The url defined below is not related to the context, being just here to perform real ajax fetch requests. To simulate if the result is ok or not, you may play with the boolean constants vars `ajaxCity`, `ajaxJourney`, `ajaxRental`.

[Try this code on jsfiddle](https://jsfiddle.net/kiwipal/toj0s2gf/7/)

```javascript
const url = "https://api.kiwipal.com/public/getCurrencyRate";
	
const ajaxCity = true;
const ajaxJourney = true;
const ajaxRental = true;
	
const waiter = (duration) => {
    return new Promise(resolve => {
            setTimeout(resolve, duration);
        }
    );
}
	
const getTravelInfos = async () => {
    return {
        id_city: null,
        id_journey: null,
        id_rental: null
    }
}
	
const getCity = async (obj) => {
    const result = await fetch(url);
    obj.id_city = (ajaxCity ? 12 : false);
    console.log('city fetched !')
    return obj;
}

const getRental = async (obj) => {
	await waiter(5000);
    const result = await fetch(url);
    console.log('rental fetched !')
    obj.id_rental = (ajaxRental ? 44 : false);
    return obj;
}
	
const getJourney = async (obj) => {
    const result = await fetch(url);
    obj.id_journey = (ajaxJourney ? 33 : false);
    console.log('journey fetched !')
    return obj;
}
	
getTravelInfos()
    .then(data => getCity(data))
    .then(data => getRental(data))
    .then(data => getJourney(data))
    .then(data => {console.log(data)});
```
	    
### Multiple URLs fetch requests

Fetch multiple URLs returning JSON data thanks to promises then return an array of results objects.

> Any failing request will stop the entire process, so you may consider using `Promise.allSettled` instead.

[Try this code on jsfiddle](https://jsfiddle.net/kiwipal/85b39u0x/6/)

```javascript
const urls = [
    "https://api.kiwipal.com/public/getCurrencyRate",
    "https://api.kiwipal.com/public/getCurrencyRate"
]
	
Promise.all( urls.map( url => fetch(url) ) )
    .then(
        results => Promise.all(
            results.map( result => result.json() )
        )
    )
    .then(
        results => console.log(results)
    );
```

### Multiple URLs fetch requests (allSettled)

Fetch multiple URLs returning results for every request even the error ones.

[Try this code on jsfiddle](https://jsfiddle.net/kiwipal/4ogt7akv/6/)

```javascript
const urls = [
    "https://api.kiwipal.com/public/getCurrencyRate",
    "https://api.kiwipal.com/public/getCurrencyRate"
]

Promise.allSettled( urls.map(url => fetch(url)) ).then(results =>
  console.log(results)
); 
```

### Chain multiple URLs fetch requests (detailled example)

Chain asynchronous promises defined in an array using `Promise.all`.

Each request returns a promise (with its `resolved` and `reject` functions).

Calls to the `waiter` function simulate slow servers transactions with various delays, to demonstrate that all requests are processed in parallel, with a final result consolidating all the results in an array.

> The order of the array values will stay the same, no matter the time required to return all results.

[Try this code on jsfiddle](https://jsfiddle.net/kiwipal/rouejnvy/6/)

```javascript
const url = "https://api.kiwipal.com/public/getCurrencyRate";
	
const ajaxCity = true;
const ajaxJourney = true;
const ajaxRental = true;
	
const waiter = (duration) => {
    return new Promise(resolve => {
            setTimeout(resolve, duration);
        }
    );
}
	
const getCity = async () => {
	
    await fetch(url);
    await waiter(1000);
	
    return new Promise(
        (resolve, reject) =>
            {
                console.log("city fetched");
                ajaxCity
                    ? resolve({id_city:12})
                    : reject({id_city: null});
            }
    ).catch(e => {
        console.log("Missing city");
        return e;
    })
}

const getRental = async () => {
	
    await fetch(url);
    await waiter(2000);
	
    return new Promise(
        (resolve, reject) =>
            {
                console.log("rental fetched");
                ajaxRental
                    ? resolve({id_rental: 44})
                    : reject({id_rental: null});
            }
    ).catch(e => {
        console.log("Missing rental");
        return e;
    })
}
	
const getJourney = async () => {
	
    await fetch(url);
    await waiter(3000);

    return new Promise(
        (resolve, reject) =>
            {
                console.log("journey fetched");
                ajaxJourney
                    ? resolve({id_journey: 33})
                    : reject({id_journey: null});
            }
    ).catch(e => {
        console.log("Missing journey");
        return e;
    })
}
	
Promise.all(
    [
        getCity(),
        getRental(),
        getJourney()
    ]
).then(obj => console.log(obj));
```
		    
## Decorators

### Decorate with high-order function

Not really a decorator, but a basic sum function decorated to return a formated string.

> The `apply` function calls a function passing a `this` environment and an array of arguments.

[Try this code on jsfiddle](https://jsfiddle.net/kiwipal/td3k2qoh/3/)

```javascript
const add = (a,b) => a + b;
	
const explain = (functionToDecorate) => {
    return function() {
    	let result = functionToDecorate.apply(this, arguments) 
        return `${arguments[0]} + ${arguments[1]} = ${ result }`
    }
}
	
const explainAdd = explain(add);

console.log( explainAdd(2,3) );
```

### Decorate with high-order function (other example)

Another version of a basic decorator.

[Try this code on jsfiddle](https://jsfiddle.net/kiwipal/soz0vwuL/48/)

```javascript
const getName = name => name.charAt(0).toUpperCase() + name.slice(1);

const emperorSalute = (func) => {
    return function () {
        let name = func.apply(this, arguments)
        return `${name} is the emperor`;
    }
}

const getEmperorSalute = emperorSalute(getName);

console.log(getName("Cesar"));
console.log(getEmperorSalute("Cesar"));
```

### Classic example

Decorate a list of persons, replacing title abbreviations wit appropriate words.  

> The `apply` function calls a function with a `this` environment and an array of arguments.

[Try this code on jsfiddle](https://jsfiddle.net/kiwipal/nom4bvh0/3/)

```javascript
const persons = [
    {title: "m", name: "Alexandre"},
    {title: "ms", name: "Diane"}
]
	
const titles = {
    m: "Mister",
    ms: "Miss"
}
	
const person = (title, name) => `${title} ${name}`;
	
const personDecorator = (functionToWrap) => {
    return function () {
        arguments[0] = titles[arguments[0]];
        return functionToWrap.apply(this, arguments);
    }
};
	
const getPerson = personDecorator(person);
	
const getDecoratedPersons = () => persons.map(
    person => getPerson(person.title, person.name)
);
	
console.log(getDecoratedPersons());
```

## Tips and Tricks

### Array navigation 

Iterate through an array without next or prev methods thanks to a modulo trick.  

A modulo calculated with a right value superior to the left value
will always return the left value, except on zero. And when left and right are the same, result is zero.

In the example below, going beyond the end of the array length will resume to the zero position, while going before the end of the array will return the last position.

> Using `++` before a variable name imediately increment a value.

[Try this code on jsfiddle](https://jsfiddle.net/kiwipal/ydxtqcL2/8/)

```javascript
const letters = ['A', 'B', 'C', 'D', 'E'];
let state = 4;
	
console.log(
    letters[ state ],
    letters[ ++state % letters.length ],
    letters[ --state % letters.length ]
);
```

### Spread operator

The spread operator is useful to add or update key/value. 

> Common in reactJS when using functional programming (hooks with states to update).

[Try this code on jsfiddle](https://jsfiddle.net/kiwipal/wdpy84qo/5/)

```javascript
let person = {
    name: "Alexander",
    spirit: "sad",
    job: "developper"
}
	
const updateObject = (obj, key, value) => ({
        ...obj,
        [key]: value
    });
	
person = updateObject(person, 'spirit', 'happy');
	
console.log(person);
```

### Spread operator with function arguments

The spread operator is also useful to add unlimited arguments in an array.

[Try this code on jsfiddle](https://jsfiddle.net/kiwipal/q9du407x/3/)

```javascript
function addCities(city, ...extraCities){
    console.log(city, cities);
}

addCities('Auckland', 'Wellington', 'Tauranga');
```

### Destructuring variables creation

Create variables from an object using the destructuring method.

> Useful when manipulating event.target from DOM.

[Try this code on jsfiddle](https://jsfiddle.net/kiwipal/ny1hraqv/2/)

```javascript
const input = {
    name: "first_name",
    value: "Guillaume"
}

const {name, value} = input;

console.log(name, value)
```

### Bitwise operator

The `NOT bitwise` operator, also known as the tilde `~` can be used as a shortcut for methods returning `-1` values. 
For example, the `indexOf` method returns a `-1` value in case of no matching values in an array.

> On MacOS, the shortcut of the tilde is `OPTION + N`.

[Try this code on jsfiddle](https://jsfiddle.net/kiwipal/yctse48g/2/)

```javascript
const city = ['Auckland', 'Wellington', 'Tauranga'];
	
console.log(
    ~city.indexOf('Auckland') ? 'Found' : 'Missing',
    ~city.indexOf('Chirstchurch') ? 'Found' : 'Missing',
);
```

### Mandatory parameters

Without Typescript or ReactJS propTypes, mandatory parameters can be defined using a function default value.

[Try this code on jsfiddle](https://jsfiddle.net/kiwipal/6mx4shdg/3/)

```javascript
const mandatory = (param) => {
    throw new Error(`Parameter: ${param} is missing`);
}
	
const getName = ( name = mandatory("name") ) => console.log(name)
	
getName("Alexandre");
getName();
```

### Ternary operator

A classic to assign differents values according to a condition.

[Try this code on jsfiddle](https://jsfiddle.net/kiwipal/z582a7ef/1/)

```javascript
const isActivated = true;

const button = isActivated ? 'on' : 'off';

console.log(button);
```

### Logical operator ||

The operator `||` is useful to assign a default value if the first one is missing. However, the trick works for objects properties but won't work for variables and is prone to errors if the first value is a boolean or is null, undefined, equal to 0 or empty...

Since 2020, one can use the logical operator `??` instead.

> In the example below, the default value will be used for the `favoriteCity2`.

[Try this code on jsfiddle](https://jsfiddle.net/kiwipal/g9Lr4vst/4/)

```javascript
const defaultCity = 'Auckland';

let city1 = {name:'Queenstown'}
let city2 = {title:'Picton'}

let favoriteCity1 = city1.name || defaultCity;
let favoriteCity2 = city2.name || defaultCity;

console.log(favoriteCity1, favoriteCity2);
```

### Logical operator ??

This logical operator partially fix the problems with the `||` operator. The first value will be assigned except if null or undefined.

> In the example below, the second example will stay empty and won't receive the defaultIp (kind of stupid, but just to illustrate the difference).

[Try this code on jsfiddle](https://jsfiddle.net/kiwipal/jo4Ldv8p/8/)

```javascript
const defaultIp = '127.0.0.1';

let server1 = {ip:'168.0.0.1'}
let server2 = {ip:''}

let myServer1 = server1.ip ?? defaultIp;
let myServer2 = server2.ip ?? defaultIp;

console.log(myServer1, myServer2);
```

[Try this code on jsfiddle](xxx)

```javascript
const defaultCity = 'Auckland';

let city1 = {name:'Queenstown'}
let city2 = {title:'Picton'}

let favoriteCity1 = city1.name || defaultCity;
let favoriteCity2 = city2.name || defaultCity;

console.log(favoriteCity1, favoriteCity2);
```

## Inheritance VS Composition

### Inheritance with extended class

A catalog class extended by a media type class which is also extended by a book class.

> Getters and setters simplify the class.

[Try this code on jsfiddle](https://jsfiddle.net/kiwipal/jrbfp81h/2/)

```javascript
class Catalog {
	
    constructor(name) {
        this._name = name;
        this._medias = [];
    }
	
    get name () {
        return this._name;
    }
	
    set add (media) {
        this._medias.push(media);
    }
	
    get medias () {
        return this._medias;
    }
}
	
class Media extends Catalog{
	
  constructor(mediaType="", author="", title="", ratings=[]) {
    super(mediaType);
    this._mediaType = mediaType;
    this._author = author;
    this._title = title;
    this._ratings = ratings;
    this._isCheckedOut = false;
  }
	
  get mediaType() {
    return this._mediaType;
  }
	
  get title() {
    return this._title;
  }
	
  get isCheckedOut() {
    return this._isCheckedOut;
  }
	
  get ratings() {
    return this._ratings.join(",");
  }
	
  set checkedOut(state = false) {
    this._isCheckedOut = state
  }
	
  toggleCheckOutStatus() {
    this._isCheckedOut = !this._isCheckedOut;
  }
	
  addRating(rating = 0) {
    this._ratings.push(rating);
  }
}
	
class Book extends Media {
  constructor(author, title, pages, ratings) {
    super("Book", author, title, ratings);
    this._pages = pages;
    this._title = title;
    this._ratings = ratings;
    this._pages = pages;
    this._author = author;
  }
}
	
myCatalog = new Catalog("TOP Sells");
	
myCatalog.add = new Book(
    author = "John Kennedy Tool",
    title = "A confederacy of dunces",
    pages = 300,
    ratings = ['Great!', 'Whoooo']
);
	
myCatalog.add = new Book(
    author = "Cicero",
    title = "De Oratorio",
    pages = 330,
    ratings = ['Superb!', 'Loved it.']
);
	
for ([key, value] of myCatalog.medias.entries()) {
    value.title == "De Oratorio" || value.toggleCheckOutStatus();
    console.log(`
        ${value.mediaType} : ${value.title}
        (checkedOut = ${value.isCheckedOut ? 'yes' : 'no'})
        `
    );
}
```

### Object composition

Composition is often better than inheritance, and this example illustrates how a book or movie objects could be defined using common methods.

[Try this code on jsfiddle](https://jsfiddle.net/kiwipal/68eh2b7d/)

```javascript
const rater = (state) => ({
  getRating() {
    return `${state.stars} stars`;
  },
  setRating(stars) {
      state.stars = stars;
  }
});

const pricer = (state) => ({
  getPrice() {
    return `${state.price} $`;
  },
  setPrice(price) {
    state.price = price;
  }
});

const discounter = (state) => ({
  discount(rebate) {
    state.price -= rebate;
  }
});

const reader = (state) => ({
  read() {
    return `${state.title} is being read on a sofa.`
  }
});

const projecter = (state) => ({
  project() {
    return `${state.title} is being projected on the screen.`
  }
});

function Book (author, title) {

  let obj = {
    type: 'Book',
    author,
    title,
    stars:0,
    price:0
  }

  return Object.assign(
    obj,
    rater(obj),
    pricer(obj),
    discounter(obj),
    reader(obj),
  )
}

const ciceron = new Book('Cicero', 'Catilina');
ciceron.setRating(4);
ciceron.setPrice(16.5);
ciceron.discount(3.5);
console.group("Book");
console.log(ciceron.getRating());
console.log(ciceron.getPrice());
console.log(ciceron.read());
console.groupEnd("Book");

function Movie (author, title) {

  let obj = {
    type: 'Movie',
    author,
    title,
    stars:0,
    price:0
  }

  return Object.assign(
    obj,
    rater(obj),
    pricer(obj),
    projecter(obj),
  )
}

const kurosawa = new Movie('Kurosawa', 'Rashomon');
kurosawa.setRating(5);
kurosawa.setPrice(29.5);
console.group("Movie");
console.log(kurosawa.getRating());
console.log(kurosawa.getPrice());
console.log(kurosawa.project());
console.groupEnd("Movie");
```

## Objects

### The [Object.create] method

Using `Object.create` is better to be used when dealing with an object maint to evolves in it's own way. Here is an example to create two objects (book and movie) based on the same (media) object.

Checking the second `console.log` reveals that movie does not seem to have a price, but in reality it has the original media price as the third `console.log` will demonstrate!

> The `Object.create` basically create an object and copy the original prototype   to an upper prototype level, allowing each created object to be a real copy and not a reflection.

[Try this code on jsfiddle](https://jsfiddle.net/kiwipal/taoq2mrL/3/)
  
```javascript
const media = {
    title: "",
    price: "is unknown for the moment."
};

const book = Object.create(media);
const movie = Object.create(media);

book.title = "How to learn magic"
book.pages = 200;
book.price = 12;

movie.title = "Star Wars III";
movie.duration = 120;

console.log("Book", book);
console.log("Movie", movie);
console.log("Movie price", movie.price)
```

### The [new Object] method

Almost same code as in example 1, but with the `new Object` method.

This time, both objects being created without their own prototype levels, any altered or new properties are in common, and the result are clones!

> Consider the `new Object` method useful to create an alias of an object to be used only once in the code.

[Try this code on jsfiddle](https://jsfiddle.net/kiwipal/1geauqcf/2/)

```javascript
const media = {
    title: "",
    price: "is unknown for the moment."
};

const book = new Object(media);
const movie = new Object(media);

book.title = "How to learn magic"
book.pages = 200;
book.price = 12;

movie.title = "Star Wars III";
movie.duration = 120;

console.log("Book", book);
console.log("Movie", movie);
```
    
### Updating an original object

Adding properties to an original object will make these properties availables to any future object created with the `new Object` or `Object.create` methods.

[Try this code on jsfiddle](https://jsfiddle.net/kiwipal/v34sdabt/12/)

```javascript
const media = {
    title: "",
};

const book = Object.create(media);
book.title = "How to learn magic"
book.pages = 200;

media.price = 12;

console.log("Book", book);
console.log("Book price = ", book.price);
```

### Optional chaining

Optional chaining `?.` selector will return `undefined` instead of throwing an error. By default, javascript already returns `undefined` if the last element of the chain is missing, but throws a type error if a previous element of the chain is missing.

> In the example, the second `console.log` will return `undefined` thanks to optional chaining syntax.

[Try this code on jsfiddle](https://jsfiddle.net/kiwipal/9Lxdz5nu/6/)

```javascript
const cities = [
    {
        name: 'Auckland',
        type: 'city',
        gps: {
            lat: '-36.848461',
            long: '174.763336'
        },
        metas: {
            population: 1606564
        }
    },
    {
        name: 'Wellington',
        type: 'city',
    },
];

console.log(cities[0].gps.lat);
console.log(cities[1]?.gps?.lat);
```

### Object with private vars

It is possible to create an object with private vars without using classes.

> In the example below, the `balance` value is private and can only be retreived using a method.

[Try this code on jsfiddle](https://jsfiddle.net/kiwipal/59a8uys0/3/)

```javascript
function makeBankAccount() {
  
  let balance = 0;

  function getBalance() {
    return balance;
  }

  function setBalance(amount) {
    balance = amount;
  }

  return {
    getBalance,
    setBalance
  };
}

const account = makeBankAccount();
account.setBalance(100);
console.log(account.getBalance());
```

## Regex

### Sample codes

A basic collection of regex codes with a `explainRegex` method to format each test.

> Check the documentation [about regex here.](https://www.w3schools.com/jsref/jsref_regexp_m.asp)

[Try this code on jsfiddle](https://jsfiddle.net/kiwipal/cqj4fb5w/)

```javascript
const text = `Auckland (not Oackland) is a city in New Zealand with 1.6 millions (yeeeeah!) inhabitants and you can contact the mayor by e-mail at city_ack@gmail.com or by phone at 666-8888-777 in New Zealand`;

console.log(text);

const explainRegex = (text, legend, regex) => {
    console.log(
        legend + ' =>',
        regex + ' => ',
        (regex).test(text),
        text.match(regex)
    );
}

const regexList = [
    [
        "Search 'New Zealand' first occurence only",
        /New Zealand/
    ],
    [
        "Search 'New Zealand'",
        /New Zealand/g
    ],
    [
        "Search 'New Zealand' first occurence only and case-insensitive.",
        /new zealand/gi
    ],
    [
        "Search 'contact the mayor by email'",
        /contact(.*)by e-?mail/g
    ],
    [
        "Search 'by e-mail' or 'by phone' at",
        /by (e-mail|phone) at/g
    ],
    [
        "Search 'e-mail' or 'email' using the ? symbol right after the optional char",
        /e-?mail/g
    ],
    [
        "Search 'Auckland (not Oackland) is a city' with or without the text in parenthesis using `()?`",
        /Auckland (\(not Oackland\))? is a city/g
    ],
    [
        "Search if string start with 'Auckland' using the anchor hat \^",
        /^Auckland/g
    ],
    [
        "Search if string end with 'Zealand'",
        /Zealand$/g
    ],
    [
        "Search if string start with 'Auckland 'and end with 'Zealand'",
        /^Auckland(.*)Zealand$/g
    ],
    [
        "Search 'Auckland' and 'city'",
        /Auckland|city/g
    ],
    [
        "Search 'yeah!' no matter the number of 'e' in the word",
        /ye*ah!/g
    ],
    [
        "Search 'email' or 'e-mail' (multiple characters can be used inside [], it is useful to find bad spelling words)",
        /e[-]mail/g
    ],
    [
        "Search `Auckland` starting with a capital A or not",
        /[Aa]uckland/g
    ],
    [
        "Search alphanumeric chars'",
        /[A-z0-9]/g
    ],
    [
        "Shortcut to search alphanumeric chars, tirets (escaped) and underscores",
        /[\w]/g
    ],
    [
        "Shortcut to search numerics integers'",
        /[\d]/g
    ],
    [
        "Shortcut to search spaces but also tab, carriage return, line break, form feed, or vertical tab",
        /[\s]/g
    ],
    [
        "Search '1.6 millions' (ugly way to illustrate concept of chars)",
        /[0-9].[0-9]\s\w\w\w\w\w\w\w/g
    ],
    [
        "Search non-alphanumerics chars and exclude spaces",
        /[^ A-z0-9]/g
    ],
    [
        "Search non alphanumerics chars",
        /\W/g
    ],
    [
        "Search non spaces (including tabs...) chars",
        /\S/g
    ],
    [
        "Search phone number formated [xxx]-[xxxx]-[xxx] (longest and ugly way)",
        /[0-9][0-9][0-9]-[0-9][0-9][0-9][0-9]-[0-9][0-9][0-9]/g
    ],
    [
        "Search phone number formated [xxx]-[xxxx]-[xxx] (shortest way)",
        /(\d{3,5}-?)+/g
    ],
    [
        "Search phone number formated [xxx]-[xxxx]-[xxx] (the + search repeated occurences)",
        /[\d]+-[\d]+-[\d]+/g
    ],
    [
        "Search phone number formated [xxx]-[xxxx]-[xxx] (the {} specify the number of occurences)",
        /[\d]{3}-[\d]{4}-[\d]{3}/g
    ],
    [
        "Search phone number formated [xxx]-[xxx]-[xxx] with `xxx` of min 3 or 5 chars",
        /[\d]{3,5}-[\d]{3,5}-[\d]{3,5}/g
    ],
];

regexList.forEach(item => explainRegex(text, ...item ));
```

## Errors & Debug

### Catch errors

Basic try/catch/finnaly example returning an error.

[Try this code on jsfiddle](https://jsfiddle.net/kiwipal/k20c9jew/7/)

```javascript
try {
    console.info(city);
} catch (e) {
    console.error(e);
} finally {
    console.log("Done!")
}
```

### Console methods

Console has several useful methods and it is even possible to specify css styles.

> This is an [interesting post](https://www.sitepoint.com/beyond-console-log-level-up-your-debugging-skills/) to read about other advanced console features.

[Try this code on jsfiddle](https://jsfiddle.net/kiwipal/vcdb093g/5/)

```javascript
console.log('First log message to be cleared');
console.clear();

const city = {
    name: 'Wellington',
    poi: ['Te Papa', 'Botanical Garden', 'Zealandia']
}

// Check the difference...
console.log(city);
console.log({city});

console.group('Demo group'); 

  console.log('Auckland');
  console.error('City is missing');
  console.warn('Default city rating applied')
  console.table({
    'city': 'Auckland',
    'rating': 5
  });

console.groupEnd('Demo group'); 

const getCurrencyRate = async (url) => {
    
    const result = await fetch(url);
    
    if (result.ok) {
	
      let styles = 'padding:10px;background:green;color:white;border-radius:3px'; 
      console.log('%cAjax call successfull!', styles); 

      console.timeEnd('check_time_to_fetch_currency');
	
    } else {
        throw new Error("Invalid URL!");
    }
    
}

console.time('check_time_to_fetch_currency'); 
getCurrencyRate('https://api.kiwipal.com/public/getCurrencyRate');
```