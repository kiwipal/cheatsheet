# Python Cheat Sheet

**A collection of useful Python 3 sample codes**  

Each sample has a link to a Repl.it to be able to test the code online.  
Find all the [codes on my profile page](https://repl.it/@kiwipal)
 
**All results are printed to the console and not displayed directly on the page.**

Please contact me if you have questions or noticed a bug or a better way to improve the sample codes.

> Most sample codes are not commented, as the code should be self-explenatory.
> Python version was 3.8.5 when this document was created.

[[_TOC_]]

TODO : Tuples

## Operators

### Specific to Python

The usual operators common to many languages are available in Python, however, some specificities exist.

[Try this code on repl.it](https://repl.it/@kiwipal/Operators-Specific-to-Python#main.py)

```python
# The ++ or -- shortcuts  are missing in python, use instead:
a = 1

a += 1
print(a)

a -= 1
print(a)

# Boolean are capitalized in Python
a = True
b = False
print(a,b)

# The walrus operator assign and return value at the same time
print(a := 5)
print(a)
```

### Ternary operator

The classic ternary shortcu is a little more verbose in Python.

[Try this code on repl.it](https://repl.it/@kiwipal/Operators-Ternary-operator#main.py)

```python
isTasty = True
advice = "Let's go" if isTasty else "Stay away"
print (advice)

rating = 2
advice = "Let's go" if rating > 4 else "Stay away"
print(advice)
```

## Control Flow

### Conditions

The `if` and `elif` basics in python.

[Try this code on repl.it](https://repl.it/@kiwipal/Control-Flow-if-condition)

```python
man = True

if man:
  print("Man")
elif not man:
  print("Woman")
else:
  print("Unknown")
```

### For loops

The different `for` loops techniques.

[Try this code on repl.it](https://repl.it/@kiwipal/Control-Flow-For-loops)

```python
# Print from 0 to 10 (inclusive)
for i in range(11):
  print(i)

# Print from 10 to 20 (inclusive)
for i in range(10,21):
  print(i)

# Print each item from a list
cities = ['auckland', 'wellington', 'tauranga', 'wanaka']
for city in cities:
  print(city)

# Print each key/value of a dictionary
for key,value in {'auckland':5, 'wellington':3}.items():
  print("{city} has a rating of {rating}".format(city=key, rating=value))

# Print each key/value of a list
cities = ['auckland', 'wellington', 'tauranga', 'wanaka']
for i in range(len(cities)):
  print(i, "=", cities[i])

# Dictionary parsed with list comprehension including a filter 
print({key:value for key,value in {'auckland':5, 'wellington':3}.items() if key != "auckland"})
```

### For/else

It is more readable to use an `else` statement to deal with no result while iterating a list using a for loop.

The example below search for the first occurence of a case insensitive keyword in a list, and will break when founded or will print a default message.

> The `else` condition would always be called if no `break` is defined.

[Try this code on repl.it](https://repl.it/@kiwipal/Control-Flow-For-loops)

```python
cities = ['Auckland', 'Wellington', 'Tauranga', 'Wanaka']

searchCity = "Auckland"

for city in cities:
    if city.lower() == "auckland":
        print("Found " + city)
        break
else:
  print("No city found ...")
```

### While loop

The `while` loop technique including `continue` and `else` control flow.

[Try this code on repl.it](https://repl.it/@kiwipal/Control-Flow-While-loop)

```python
cities = [
  {"name": "auckland", "country": "nz"},
  {"name": "paris", "country": "fr"},
  {"name": "welllington", "country": "nz"}
]

countryToKeep = "nz"
excludedCities = []

print("Cities in "+ countryToKeep.upper() +":\n")

while len(cities):
  city = cities.pop()
  if (city.get("country") != countryToKeep):
    excludedCities.append(city.get("name"))
    continue
  print("- " + city.get("name").title())
else:
  if (len(excludedCities)):
    print("\nCities not in "+ countryToKeep.upper() +":\n")
    while len(excludedCities):
      print("- " + excludedCities.pop().title())
```

## Variables

### Assign

It is possible to declare different variables at the same time.
 
[Try this code on repl.it](https://repl.it/@kiwipal/Variables-Assign)

```python
auckland, dunedin, wellington = 'north', 'south', 'north'
print(auckland, dunedin, wellington)
```

### Check format

Check if vars (number, list, dict...) format.

> Modules `re` for regex is needed to check emails format and module `number`
 is great to check numbers.
 
[Try this code on repl.it](https://repl.it/@kiwipal/Variables-Check-format#main.py)

```python
import re
import numbers

# Is a number
x = 5
print(isinstance(x, numbers.Number))

# Is integer (float will return false)
x = 1.0
print( isinstance(x, int) )

# Is integer
x = 1
print( isinstance(x, int) )

# Is list
x = [1,2,3]
print( isinstance(x, list) )

# Is dictionary
x = {"city": "auckland"}
print( isinstance(x, dict) )

# Is string
x = "wanaka"
print( isinstance(x, str) )

# Is email
email = "xxx@gmail.com"
print( bool( re.search(r"^[\w\.\+\-]+\@[\w]+\.[a-z]{2,3}$", email) ) )

# Is key defined in a dictionary
x = {"city": "auckland"}
print( "city" in x.keys() )

# Is index of a list
x = [1,2,3]
try:
  x[1]
except IndexError:
    print('missing index')
```

### Constants

There is no implementations of constants in Python, therefore we use a dedicated module to store the constants with its uppercase convention.

> Using a `constant.XXX` does not protect the constant variable to be updated, but limit the risk of misusing (for example there is less risk to accuidentally use `constant.C` instead of `c`)
 
[Try this code on repl.it](https://repl.it/@kiwipal/Variables-constants#constant.py)

**constant.py**

```python
PI = 3.14
GRAVITY = 9.8
```

**main.py**


```python
import constant

constant.PI = 3

print(
  constant.PI,
  constant.GRAVITY
)
```

## Strings

### String quotes

Different ways to store text in Python's strings.

[Try this code on repl.it](https://repl.it/@kiwipal/Strings-Quotes#main.py)

```python
a = "Can't wait to see \"LOTR\""
b = '\nCan\'t wait to see "LOTR"'

# Multiline
c = """
Can't wait
to see "LOTR"
"""

d = '''
Can't wait
to see "LOTR"
'''

print(a,b,c,d)
```

### Format strings

Python offers many strings methods, and here are the most frequently used.

> Check the [official documentation](https://www.w3schools.com/python/python_ref_string.asp) for the entire list of methods.

[Try this code on repl.it](https://repl.it/@kiwipal/Strings-Format#main.py)

```python
s = "auckland is a nice city"

print(s.title())
print(s.upper())
print(s.lower())
print(s.count("auckland"))
print(s.endswith("city"))
print(s.find("nice"))
print(s.strip("city"))

# Print {auckland} {is a nice} {city}
print(
  s[0:8],
  s[9:18],
  s[-4:]
)

# String and numbers require methods to be concatenated or summed
print("King Alexander " + str(2))
print(2 + int("2"))
```

### Old format methods

Format method can be used in many different traditional ways.

[Try this code on repl.it](https://repl.it/@kiwipal/Strings-old-format-methods#main.py)

```python
# Sprintf kind
print(
  "%s is rated %d" % ("Auckland", 5)
)

# Format method
print( "I love {0} and {1}".format("auckland", "wellington") )

print(
  "{city} is rated {rating}".format(
    city = "Auckland",
    rating = 5
  )
)

# Format method also has some special tricks
print("The universe is {:,} years old.".format(13800000000))
```

### New format method

The `F` or `f` method was introduced in Python 3.6 to format strings with an even more elegant syntax allowing to use variables, operations and even function calls to be directly used in curly braces.

[Try this code on repl.it](https://repl.it/@kiwipal/Strings-f-method#main.py)

```python
country = "new zealand"

print( f"I love { country.title() }" )

print(
  f"""
  I love Peter Jackson's movies
  filmed in { country.upper() }
  """
)

def explainSum(a,b):
  return f'{a} + {b} = {a + b}'

print( f"I'm pretty sure that { explainSum(2,2) }" )

print( f"I'm pretty sure that 2 + 2 = {2+2}" )
```

## List

### List methods

Basic methods to manipulate lists with `append`, `insert`, `pop` ...

[Try this code on repl.it](https://repl.it/@kiwipal/List-Methods#main.py)

```python
# List can contain anything (dict, integer, list, tuple, string, lambda, function)
print([
  5,
  "Alexander",
  [1,2,3],
  {"id":1},
  (3,4,5),
  lambda a,b: a+b
])

# Merge
a = [1,2,3]
b = [4,5,6]
print(a + b)

# Merge and sort
a = [1,3]
b = [2]
print(sorted(a + b))

# Copy list to create brand new list
a = [1,2,3]
b = a.copy()
b.append(4)
print(a)
print(b)

# Append element
cities = ["Auckland"]
cities.append("Wellington")
print(cities)

# Insert element
cities.insert(1, "Wanaka")
print(cities)

# Index of first occurence starting at zero
print(cities.index("Wellington", 0))

# Count the number of occurence
print(cities.count("Auckland"))

# Remove element by its value
cities.remove("Wanaka")
print(cities)

# Remove last element and return its value
print(cities.pop())

# Remove index element and return its value
print(cities.pop(0))
```

### List Zip

Merge multiple list to create an iterable of tuples that can be converted to a list using the map function.

[Try this code on repl.it](https://repl.it/@kiwipal/List-Zip#main.py)

```python
cities = ["Auckland", "Wellington", "Wanaka"]
ratings = [3, 5, 2]

result = zip(cities,ratings)
print(list(map(list, result)))
```

### List filter

Define custom filter function returning a boolean to be used with the `filter` method on a list.

> It is also possible to use a lambda function.

[Try this code on repl.it](https://repl.it/@kiwipal/List-Filter#main.py)

```python
# Default Function version
numbers = [1, 2, 3, 4]

def lessThan(number):
	return number < 3

filterdNumbers = list(filter(lessThan, numbers))
print(filterdNumbers) 

# Lambda version
cities = ["Auckland", "Wellington", "Wanaka"]
filterdCities = list(filter(lambda i : len(i) < 10, cities))
print(filterdCities)
```

### List map

Use the list `map` method to iterate a list while applying a function on each item. 

> It is also possible to use a lambda function.

[Try this code on repl.it](https://repl.it/@kiwipal/List-map#main.py)

```python
# Using a function
cities = ["auckland", "wellington", "wanaka"]

def formatCity(city):
  return city.title() + " in NZ"

print(list(map(formatCity, cities)))

# Using a lambda
print(list(map(lambda i:i.title() + " in NZ" , cities)))
```

### List enumerate

Use the list `enumerate` method to iterate a list fetching both values and incrementing a counter. 

> It is also possible to define the counter initial value.

[Try this code on repl.it](https://repl.it/@kiwipal/List-enumerate)

```python
cities = ["auckland", "wellington", "wanaka"]

for counter,city in enumerate(cities, 1):
  print(counter, city.title())
```

## Dictionary

### Dictionary methods

How to manipulate a dictionary to sort, add, update, remove items ...

[Try this code on repl.it](https://repl.it/@kiwipal/Dictionary-Operations#main.py)

```python
data = {
  "auckland": {
    "id": 22,
    "island": "north",
    "rating": 5
  },
  "wanaka": {
    "id": 43,
    "island": "south",
    "rating": 4
  },
  "tauranga": {
    "id": 56,
    "island": "north",
    "rating": 4
  }
}

data2 = {
    "hamilton": {
    "id": 88,
    "island": "north",
    "rating": 3
  }
}

# Copy data content to a new city var
cities = data.copy()

# Another way to copy datas content, with multiple datas if needed (same process as used in kwargs)
cities = {**data, **data2}

# Return and remove first item
print(data.popitem())

# Return and remove specific item
print(data.pop("wanaka"))

# Sort a list by key names
print({key:value for key,value in sorted(cities.items())})

# Return Wanaka rating
print(cities.get("wanaka").get("rating"))

# Custom returned value if missing key
print(cities.get("christchurch", "missing key!"))

# Add new city
cities["whangarei"] = {
  "id": 18,
  "island": "North",
  "rating": 5
}

# Update a dictionary by key (add a key/value without removing other pair)
cities["whangarei"].update({
  "rating": 4,
  "description": "nice city!"
})

# Delete an existing key
del cities["whangarei"]

# Delete a key using `pop` method (when not sure of the key existence), passing a default return value as `None`
cities.pop("whangarei", None)
```

### Sort a list of dictionaries

The imported `itemgetter` operator can be used to sort on multiple values in a list of dictionaries.

> It would be more logical to request sort when fetching datas from a database, but in some cas uyou might need to manipulate unsorted datas from different sources. 

[Try this code on repl.it](https://repl.it/@kiwipal/Dictionaries-Sort-a-list-of-dictionaries)

```python
from operator import itemgetter

cities = [
  {
    "id": 1,
    "name": "Wellington",
    "island": "north",
    "rating": 3
  },
  {
    "id": 2,
    "name": "Wanaka",
    "island": "South",
    "rating": 4
  },
  {
    "id": 3,
    "name": "Auckland",
    "island": "north",
    "rating": 5
  }
]

# Sort in reverse order
cities = sorted(
   cities,
   key=itemgetter('island', 'rating'),
   reverse=True
)

print(cities)
```

## List Comprehensions

### Range list creation

Dynamic creation of lists using the `range` function.

[Try this code on repl.it](https://repl.it/@kiwipal/List-comprehension-range-list-creation)

```python
# List of even values between 0 an 10
print([i for i in range(0, 12) if i % 2 == 0])

# Dictionary with number keys ans odd/even values.
print({i:("odd" if i % 2 else "even") for i in range(0, 12)})
```

### Dictionary operations

List comprehensions are useful to create new lists or to update them.

> The dictionary methods `items`, `keys` or `values` are useful in list comprehensions.

[Try this code on repl.it](https://repl.it/@kiwipal/List-comprehension-dictionary-operations)

```python
cities = {
  "auckland": {
    "id": 22,
    "island": "north",
    "rating": 5
  },
  "wanaka": {
    "id": 43,
    "island": "south",
    "rating": 4
  },
  "tauranga": {
    "id": 56,
    "island": "north",
    "rating": 4
  }
}

# List of city names
print([i for i in cities.keys()])

# List of north cities names
print([key for key,value in cities.items() if value.get('island') == "north"])

# Dictionary of south cities
print({key:value for key,value in cities.items() if value.get('island') == "south"})

# Dictionary of cities with rating > 4
print({key:value for key,value in cities.items() if value.get("island") == "north" if value.get("rating") > 4})

# Set rating for items with specific ids
{key:value for key,value in cities.items() if value.get("island") =='north'}

# Capitalize island
[cities[key].update({"island": value.get("island").title()}) for key, value in cities.items()]
print(cities)
```

## Useful methods

### JSON methods

Call the method `json.dumps` to convert a dictionary to JSON with optional formating options, then call `json.loads` to convert JSON to a dictionary.

> Module `json` is required

[Try this code on repl.it](https://repl.it/@kiwipal/Useful-methods-JSON)

```python
import json

cities = {
  "tauranga": {
    "id": 56,
    "island": "north",
    "rating": 4
  },
  "auckland": {
    "id": 22,
    "island": "north",
    "rating": 5
  },
  "wanaka": {
    "id": 43,
    "island": "south",
    "rating": 4
  }
}

citiesJSON = json.dumps(cities, sort_keys=True, indent=2)
print(citiesJSON)

cities = json.loads(citiesJSON)
print(cities)
```

### Random methods

Different useful random operation to pick random value or random items from a list.

> Module `random` is required

[Try this code on repl.it](https://repl.it/@kiwipal/Useful-methods-Random)

```python
import random

# Return float between 0 and 1
print( random.random() )

# Return random integer from 0 to 100 (inclusive)
print( random.randint(0,100) )

# Return a random item from a list
print( random.choice(['a', 'b', 'c']) )

# Return x values from list (2 in the example below)
print( random.sample(['a', 'b', 'c', 'd'], 2) )

# Shuffle a list
letters = ['a', 'b', 'c', 'd', 'e']
random.shuffle(letters)
print(letters)

# Return an even integer between 0 and 10
print( random.randrange(0, 12, 2) )

# Return an odd integer between 1 and 9
print( random.randrange(1, 11, 2) )
```

## Files

### File methods

Writing and reading content in a file using `with` to automatically close the file after block code.

> Modules `json` and `os` are required to manipulate json and delete files.

[Try this code on repl.it](https://repl.it/@kiwipal/Useful-methods-Files)

```python
import json
import os

filename = "cities.json"

cities = {
  "paris": {
    "rating": 5, "country": "fr"
  }
}

with open(filename, 'w', encoding = 'utf-8') as file:  
  json.dump(cities, file)

with open(filename, 'r', encoding = 'utf-8') as file:
  content = file.read()
  cities = json.loads(content)

print(cities.get("paris").get("country"))

if os.path.exists(filename):
  os.remove(filename)
```

### File methods (other example)

Writing, appending then reading content and store it to a string or to a list of lines.

> Modules `json` and `os` are required to manipulate json and delete files.

[Try this code on repl.it](https://repl.it/@kiwipal/Useful-methods-Files-other-example#main.py)

```python
import json
import os

filename = "cities.txt"
cities = ["Wanaka", "Auckland", "Tauranga"]

with open(filename, "w", encoding="utf-8") as file:
  for city in cities:
    file.write(city + "\n")

with open(filename, "a", encoding="utf-8") as file:
  file.write("Hamilton")

with open(filename, "r", encoding="utf-8") as file:
  cities = file.read()
  print(cities)

with open(filename, "r", encoding="utf-8") as file:
  cities = (file.read().splitlines())
  for city in cities:
    print(city.upper())

if os.path.exists(filename):
  os.remove(filename)
```

### Directories and permissions

Creating and deleting files and directories while granting access.

> Module `os` is required to manipulate files and folders and the module `shutil ` allows to easily create subfolders and remove them.

[Try this code on repl.it](https://repl.it/@kiwipal/Files-Directories-and-permissions#main.py)

```python
import os
import shutil

# Current dir
print ("The current working directory is %s" % os.getcwd())

path = "auckland"

# Create, chmod and remove a dir
os.mkdir(path)
os.chmod(path, 0o0666)
os.rmdir(path)

# Create and remove subdirs 
os.makedirs(path + "/restaurants/bestof/italians")
shutil.rmtree(path)
```

### Fetch url

Fetch Kiwipal currency API to get NZD vs EUR.

> Modules `urlopen` and `json` are required in this example.

[Try this code on repl.it](https://repl.it/@kiwipal/File-Fetch-url#main.py)

```python
import json
from urllib.request import urlopen

with urlopen("https://api.kiwipal.com/public/getCurrencyRate") as page:
  print(
  "1 EUR in NZD :",
  json.loads( page.read() )
    .get('response')
    .get('nzd')
)
```

## Functions

### A basic function

Including default values for parameters, raising an error in case of type error.

[Try this code on repl.it](https://repl.it/@kiwipal/Functions-Basic)

```python
def add(a=0, b=0):
    try:
        a + b
    except TypeError:
        print('Invalid variables types')
    return a + b
    
print(add())
print(add(2,2))
print(add("2", 2))
```

### Lambda function

Basically just a one line anynomous function shortcut.

[Try this code on repl.it](https://repl.it/@kiwipal/Functions-Lambda#main.py)

```python
makeKing = lambda name: "King {name}".format(name=name.title())
print( makeKing("william") )
```

### High order function

Not so far from a decorator, just to illustrate the concept.

[Try this code on repl.it](https://repl.it/@kiwipal/Functions-High-order-function#main.py)

```python
def add (a=0, b=0):
  return a + b

def explainAdd(func):
  def explain (a,b):
    text = '{a} + {b} = {sum}'.format(
      a = a,
      b = b,
      sum = func(a,b)
    )
    return text
  return explain

addText = explainAdd(add)

print(add(2,2))
print(addText(2,2))
```

### High order function (variant)

Same method than previous one, but using a lambda to shorten the code

[Try this code on repl.it](https://repl.it/@kiwipal/Functions-High-Order-Function-Variant#main.py)

```python
def add (a=0, b=0):
  return a + b

def explainAdd(func):
  return lambda a,b: '{a} + {b} = {sum}'.format(
    a = a,
    b = b,
    sum = func(a,b)
  )

addText = explainAdd(add)

print(add(2,2))
print(addText(2,2))
```

### Function caching

The result of functions can be cached to improve performance

[Try this code on repl.it](https://repl.it/@kiwipal/Functions-Caching)

```python
from functools import lru_cache

@lru_cache(maxsize=32)
def makeKing(name):
    return "%s the Great" % name

print(makeKing("Alexander"))
```

## Decorators

### Basic function decorator

A classic decorator, which is basically a high order function applied with an @function-name before the function to decorate.

> Note the `*args` to get the function parameters without having to specify their names.
 
[Try this code on repl.it](https://repl.it/@kiwipal/Decorators-function-example#main.py)

```python
def explainAdd(func):
  def explain (*args):
    text = '{a} + {b} = {sum}'.format(
      a = args[0],
      b = args[1],
      sum = func(args[0], args[1])
    )
    return text
  return explain

@explainAdd
def add (a=0, b=0):
  return a + b

print( add(2,2) )
```

### Class decorator

A basic `add` sum function with a decorator to perform a log and another decorator to format the display. Finally, a comprehension list is used to display five `add` function calls with random values.

The `debug` class is meant to be generic and is not diretcly related to the decorators. In this example, the  `debug` class is extended by the class  `debugAddDecorator` which is used as a decorator.

> Instead of real logging, the log is printed for demonstration.

[Try this code on repl.it](https://repl.it/@kiwipal/Decorators-Class-decorator#main.py)

```python
from datetime import datetime
import random

class debug:

  def __init__(self, functionName):
    self.functionName = functionName
    self.count = 0

  def getCount(self):
    self.count += 1
    return self.count

  def getDatetime(self):
    today = datetime.today()
    return today.strftime("%Y-%m-%d %H:%M:%S")

  def getFunctionName(self):
    return self.functionName

  def logMessage(self, *args):

    message = {
      "call" : self.getCount(),
      "function" : self.getFunctionName(),
      "params": args,
      "date" : self.getDatetime()
    }

    print(message)


class debugAddDecorator(debug):

  def __init__(self, functionToWrap):
    super().__init__("add")
    self.functionToWrap = functionToWrap
  
  def __call__(self, *args):
    self.logMessage(*args)
    return self.functionToWrap(*args)


class explainAddDecorator():

  def __init__(self, funcToWrap):
    pass

  def __call__(self, *args):
    text = '{a} + {b} = {sum}'.format(
      a = args[0],
      b = args[1],
      sum = sum(args)
    )
    return text
  
@debugAddDecorator
@explainAddDecorator
def add(a=0, b=0):
  return a + b

print([
  add(random.randint(0,9), random.randint(0,9))
  for item in range(0,5)
])
```

## Class

### Extended class

The `Book` class extends the `Media` class and the media type is defined automatically using the class name. Therefore, creating a new book instanciate a new `Media`.

> In this example, the mediaType is defined using `self.__class__.__name__` to get the class name (Book), because it's elegant, but passing a string could be enough.

[Try this code on repl.it](https://repl.it/@kiwipal/Class-Extended-class#main.py)

```python
class Media:

  def __init__(self, mediaType="", title=""):
    self._mediaType = mediaType
    self._title = title
    pass

  def getMediaType(self):
    return self._mediaType

  def getTitle(self):
    return self._title

class Book(Media):

  def __init__(self, title):
    super().__init__(
      mediaType = self.__class__.__name__.lower(),
      title = title
    )

myBook = Book("The New Zealand Guide")

print(
  "{title} is a {mediaType}.".format(
    title = myBook.getTitle(),
    mediaType = myBook.getMediaType()
  )
)
```

### Getters and Setters

In classes, a variable which start with a double underscore is private. It is possible to define private getter and setter methods.

The only problem with this method is that the assignement of the getter and setter must be done after their declaration. That's why another solution exists, explained in the next example.

> The `demo` function illustrate how calling a private var with or without the getter can return different values. It is often a bad idea to uses a getter instead of a private var inside a class method.

[Try this code on repl.it](https://repl.it/@kiwipal/Class-Dunder#main.py)

```python
class Book:

  def __init__(self, title):
      self.__set_title(title)

  def __get_title(self):
      return "-- {title} --".format(title = self.__title)

  def __set_title(self, title):
    if (title.count("Scotland")):
        title = title.replace("Scotland", "Beautiful Scotland")
    self.__title = title

  def demo(self):
    return (
      "Title with getter : {title}".format(title = self.title),
      "Title Without getter : {title}".format(title = self.__title)
    )

  title = property(__get_title, __set_title)

myBook = Book("History of New Zealand")

print(myBook.title)
print(myBook.demo())

myBook.title = "Study of Scotland"
print(myBook.title)

# Throw an error because trying to access a private var
# print(myBook.__title)
```

### Getters and Setters (alternate method)

The code used in the previous example, but refactored to have getter defined with the `@property` and the setter with the `@[var_name].setter`.

This solution avoid to specify the getter and setter at the end of the class and is more readable.

> The `demo` function illustrate how calling a private var with or without the getter can return different values. It is often a bad idea to uses a getter instead of a private var inside a class method.

[Try this code on repl.it](https://repl.it/@kiwipal/Class-Getter-and-Setter-alternate)

```python
class Book:

  def __init__(self, title):
      self.__title = title

  @property
  def title(self):
      return "-- {title} --".format(title = self.__title)

  @title.setter
  def title(self, title):
    if (title.count("Scotland")):
        title = title.replace("Scotland", "Beautiful Scotland")
    self.__title = title

  def demo(self):
    return (
      "Title with getter : {title}".format(title = self.title),
      "Title Without getter : {title}".format(title = self.__title)
    )

myBook = Book("History of New Zealand")

print(myBook.title)
print(myBook.demo())

myBook.title = "Study of Scotland"
print(myBook.title)

# Throw an error because trying to access a private var
# print(myBook.__title)
```

### Static methods

Static method can be called directly without creating an instance of the class.

> The `@staticmethod` is just a convention.

[Try this code on repl.it](https://repl.it/@kiwipal/Class-Static-method)

```python
class Format:
  
  @staticmethod
  def upperCase(string):
      return string.upper()

print(Format.upperCase("auckland"))
```

## Exceptions

### Custom exceptions

Extend the `Exception` class to use custom exception.

[Try this code on repl.it](https://repl.it/@kiwipal/Exceptions-Custom-exception#main.py)

```python
class FormValidationError(Exception):

  def __init__(self, code):
      message = self.demo(code)
      super().__init__(message, code)
      

  def demo(self, code=""):

    code = "default" if "" else code

    return {
        'city': "Missing city name",
        'name': "Where is the name dude?",
        'default': "Missing code value..."
    }[code]

raise(FormValidationError("city"))
```

### Try/Except

Python also offers a `else` statement called in any case. 

[Try this code on repl.it](https://repl.it/@kiwipal/Exceptions-Try-Except#main.py)

```python
def calc(b, c):
  try:
    result = b + c
  except NameError:
      return "Missing variable"
  except TypeError:
      return "Type error, please use integers"
  else:
    return result

print( calc(2, 2) )
print( calc("2", 3) )
```

## Factories

### Class factory

The `City` product class has a client to implement ratings from a factory.

The `KiwipalRating` and `MercerRating` have their own way to convert a percent to a number of stars. The methods are quite similar, but illustrate the principles of the factories.

Using the `**kwargs` operator allows to pass different arguments to each implementations.

All classes are in the same code for the demo, but a more robust code would see the classes `ObjectFactory`, `City` and both ratings implementations splited in separate module files.

> The `ObjectFactory` is a default class to register and create factories.

[Try this code on repl.it](https://repl.it/@kiwipal/Factories-Class-with-rating-factory)

```python
import math

class ObjectFactory:
  def __init__(self):
    self._builders = {}

  def register_builder(self, key, builder):
    self._builders[key] = builder

  def create(self, key, **kwargs):
    builder = self._builders.get(key)
    if not builder:
      raise ValueError(key)
    return builder(**kwargs)

class KiwipalRating:

  maxStars = 5

  def __init__(self, **kwargs):
    self.__dict__.update(kwargs)

  def convertPercentToStars(self, percent=100):
    return round((self.maxStars * percent) / 100)

class MercerRating:

  maxStars = 3

  def __init__(self, **kwargs):
    self.__dict__.update(kwargs)

  def convertPercentToStars(self, percent=100):
    return math.ceil((self.maxStars * percent) / 100)

class City:

  def __init__(self, name):
    self._name = name

  def getName(self):
    return self._name

  def setRatingFactories(self, ratingFactories):
    self._ratingFactories = ratingFactories

  def getRatingFactory(self, ratingFactory, **kwargs):
    return self._ratingFactories.create(ratingFactory, **kwargs)

# Register the factories
ratingFactories = ObjectFactory()
ratingFactories.register_builder("kiwipal", KiwipalRating)
ratingFactories.register_builder("mercer", MercerRating)

myCity = City("Auckland");
myCity.setRatingFactories(ratingFactories)

print("{name} has a {stars} stars rating on Kiwipal".format(
  name = myCity.getName(),
  stars = myCity.getRatingFactory("kiwipal", maxStars=5).convertPercentToStars(80)
))

print("{name} has a {stars} stars rating on Mercer".format(
  name = myCity.getName(),
  stars = myCity.getRatingFactory("mercer").convertPercentToStars(80)
))
```

### Function factory

Same logic as previous class factory sample code, but with functions.

> Code is splited in different files.

[Try this code on repl.it](https://repl.it/@kiwipal/Factories-Function-factory)

**main.py**

```python
from ratings.kiwipal import rating as kiwipalRating
from ratings.mercer import rating as mercerRating

def getRatingFactory(*args, **kwargs):
  builder = kwargs.get("builder")
  return ratingFactory[builder](*args, **kwargs)

ratingFactory = {}
ratingFactory["kiwipal"] = kiwipalRating
ratingFactory["mercer"] = mercerRating

cities = {
  "auckland": {
    "rating": getRatingFactory(builder="kiwipal", percent=100)
  },
  "wanaka": {
    "rating": getRatingFactory(builder="mercer", percent=80, maxStars=10)
  }
}

print(cities)
```

**ratings/kiwipal.py**

```python
def rating(percent, *args, **kwargs):
  return round((5 * percent) / 100)
```

**ratings/mercer**

```python
def rating(percent, maxStars=10, *args, **kwargs):
  return round((maxStars * percent) / 100)
```

## Design patterns

### Singleton

Often considered as an anti-pattern, the Singleton should be avoided in most case but must be known as it is still widely used.

A `metaclass` is perfect to create a Singleton as it allows to define a custom `__call__`. The metaclass holds a private class instance list, and can refuse to create additional instance.

> Singleton is great for objects that must not be multi-instanciated in any ways, like database connector, authentification ...

[Try this code on repl.it](https://repl.it/@kiwipal/Design-patterns-Singleton)

```python
class MysqlMeta(type):

  _instances = {}
 
  def __call__(cls, *args, **kwargs):
    if cls not in cls._instances:
      instance = super().__call__(*args, **kwargs)
      cls._instances[cls] = instance
    else:
      print("Warning: Singleton Instance already exists, a new instance was not created.")
    return cls._instances[cls]

class Mysql(metaclass=MysqlMeta):

  def __init__(self, db="", login="", password=""):
    self._db = db
    self._login = login
    self._password = password
    self._connect()

  def _connect(self):
    print("Connected to database!")
    pass


s1 = Mysql("kiwipal.db", "kiwi", "DD92!Dd_nsk22")
s2 = Mysql("kiwipal.db", "kiwi", "DD92!Dd_nsk22")

# Same class id, means same instance
print( id(s1), id(s2) )
```

## Debug

### Python version

Displaying the Python version in use, requires the module `sys`.
 
[Try this code on repl.it](https://repl.it/@kiwipal/Debug-Python-version#main.py)

```python
import sys
print("Python version", sys.version)
```

### Exit

Stop current script requires the module `sys`.
 
[Try this code on repl.it](https://repl.it/@kiwipal/Debug-exit#main.py)

```python
import sys

print("Hello World ?")
sys.exit()
print("Hello World !")
```

### Pretty Print

The `pprint` method is useful to display dictionaries and lists in a more readable way.

> The `json` module provides an easier and more elegant way to display dictionaries in a prettier presentation.
 
[Try this code on repl.it](xxx)

```python
import pprint
import json

pp = pprint.PrettyPrinter(
  indent=4,
  depth=2,
  sort_dicts=True
)

cities = {
  "wanaka": {
    "island": "South",
    "rating": 4,
    "gps": {
      "lat": -36.848461,
      "long": 174.763336
    }
  },
  "auckland": {
    "island": "North",
    "rating": 5,
    "gps": {
      "lat": -44.6999972,
      "long": 169.1499994
    }
  }
}

print(cities)
pp.pprint(cities)

# More elegant solution for dictionaries
print(
  json.dumps(cities, sort_keys=True, indent=2)
)
```